import { CreateStadiumDto } from "../dto/stadium/create.dto";
import { GetStadiumDto } from "../dto/stadium/get.dto";
import { UpdateStadiumDto } from "../dto/stadium/update.dto";
import { Stadium } from "../entities/stadium.entity";
export declare class ConvertStadiumMapper {
    static convertToStadium(stadium?: CreateStadiumDto): Stadium;
    static convertToGetStadiumDto(stadium?: Stadium): GetStadiumDto;
    static convertListOfStadiumsToGetStadiumDto(stadiums?: Array<Stadium>): Array<GetStadiumDto>;
    static convertUpdateToStadium(stadium?: UpdateStadiumDto, stadiumToUpdate?: Stadium): Stadium;
}
