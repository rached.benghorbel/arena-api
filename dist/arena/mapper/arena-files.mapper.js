"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArenaFilesMapper = void 0;
const file_entity_dto_1 = require("../../communs/dto/file-entity.dto");
const arena_files_entity_1 = require("../entities/arena-files.entity");
class ArenaFilesMapper {
    static toArenaFiles(file) {
        const arenaFile = new arena_files_entity_1.ArenaFiles();
        if (file.pk)
            arenaFile.pk = file.pk;
        arenaFile.content = file.content;
        arenaFile.extension = file.extension;
        arenaFile.name = file.name;
        arenaFile.path = file.path;
        arenaFile.size = file.size;
        return arenaFile;
    }
    static convertFileEntityDtoToArenaFiles(files) {
        const arenaFiles = new Array();
        files === null || files === void 0 ? void 0 : files.forEach((file) => {
            arenaFiles.push(this.toArenaFiles(file));
        });
        return arenaFiles;
    }
}
exports.ArenaFilesMapper = ArenaFilesMapper;
//# sourceMappingURL=arena-files.mapper.js.map