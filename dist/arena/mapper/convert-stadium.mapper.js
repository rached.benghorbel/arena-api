"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertStadiumMapper = void 0;
const convert_file_entity_mapper_1 = require("../../communs/mappers/convert-file-entity.mapper");
const get_dto_1 = require("../dto/stadium/get.dto");
const stadium_entity_1 = require("../entities/stadium.entity");
const convert_arena_mapper_1 = require("./convert-arena.mapper");
const stadium_files_mapper_1 = require("./stadium-files.mapper");
class ConvertStadiumMapper {
    static convertToStadium(stadium) {
        if (!stadium) {
            return null;
        }
        const newStadium = new stadium_entity_1.Stadium();
        newStadium.name = stadium === null || stadium === void 0 ? void 0 : stadium.name;
        newStadium.description = stadium.description;
        newStadium.isAvailable = stadium.isAvailable;
        newStadium.playerNumber = stadium.playerNumber;
        newStadium.arena = convert_arena_mapper_1.ConvertArenaMapper.convertToArena(stadium.arena);
        newStadium.arena.pk = stadium.arena.code;
        newStadium.pictures = stadium_files_mapper_1.StadiumFilesMapper.convertFileEntityDtoToArenaFiles(stadium.pictures);
        return newStadium;
    }
    static convertToGetStadiumDto(stadium) {
        if (!stadium) {
            return null;
        }
        const convertedStadium = new get_dto_1.GetStadiumDto();
        convertedStadium.code = stadium === null || stadium === void 0 ? void 0 : stadium.pk;
        convertedStadium.name = stadium === null || stadium === void 0 ? void 0 : stadium.name;
        convertedStadium.description = stadium.description;
        convertedStadium.playerNumber = stadium === null || stadium === void 0 ? void 0 : stadium.playerNumber;
        convertedStadium.isAvailable = stadium === null || stadium === void 0 ? void 0 : stadium.isAvailable;
        convertedStadium.pictures = convert_file_entity_mapper_1.ConvertFileEntityMapper.toDtoList(stadium === null || stadium === void 0 ? void 0 : stadium.pictures);
        return convertedStadium;
    }
    static convertListOfStadiumsToGetStadiumDto(stadiums) {
        const getStadiumsDto = new Array();
        stadiums.forEach(stadium => {
            getStadiumsDto.push(this.convertToGetStadiumDto(stadium));
        });
        return getStadiumsDto;
    }
    static convertUpdateToStadium(stadium, stadiumToUpdate) {
        if (stadium.description) {
            stadiumToUpdate.description = stadium.description;
        }
        if (stadium.name) {
            stadiumToUpdate.name = stadium.name;
        }
        if (stadium.isAvailable !== null) {
            stadiumToUpdate.isAvailable = stadium.isAvailable;
        }
        if (stadium.playerNumber) {
            stadiumToUpdate.playerNumber = stadium.playerNumber;
        }
        if (stadium.pictures) {
            stadiumToUpdate.pictures = stadium_files_mapper_1.StadiumFilesMapper.convertFileEntityDtoToArenaFiles(stadium.pictures);
        }
        return stadiumToUpdate;
    }
}
exports.ConvertStadiumMapper = ConvertStadiumMapper;
//# sourceMappingURL=convert-stadium.mapper.js.map