"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StadiumFilesMapper = void 0;
const file_entity_dto_1 = require("../../communs/dto/file-entity.dto");
const stadium_files_entity_1 = require("../entities/stadium-files.entity");
class StadiumFilesMapper {
    static toArenaFiles(file) {
        const stadiumFile = new stadium_files_entity_1.StadiumFiles();
        if (file.pk)
            stadiumFile.pk = file.pk;
        stadiumFile.content = file.content;
        stadiumFile.extension = file.extension;
        stadiumFile.name = file.name;
        stadiumFile.path = file.path;
        stadiumFile.size = file.size;
        return stadiumFile;
    }
    static convertFileEntityDtoToArenaFiles(files) {
        const stadiumFiles = new Array();
        files.forEach((file) => {
            stadiumFiles.push(this.toArenaFiles(file));
        });
        return stadiumFiles;
    }
}
exports.StadiumFilesMapper = StadiumFilesMapper;
//# sourceMappingURL=stadium-files.mapper.js.map