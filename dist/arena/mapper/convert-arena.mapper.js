"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertArenaMapper = void 0;
const convert_file_entity_mapper_1 = require("../../communs/mappers/convert-file-entity.mapper");
const get_dto_1 = require("../../users/dto/contact/get.dto");
const contact_entity_1 = require("../../users/entities/contact.entity");
const get_dto_2 = require("../dto/arena/get.dto");
const arena_entity_1 = require("../entities/arena.entity");
const arena_files_mapper_1 = require("./arena-files.mapper");
class ConvertArenaMapper {
    static convertToArena(arena) {
        var _a;
        const arenaConverted = new arena_entity_1.Arena();
        arenaConverted.name = arena === null || arena === void 0 ? void 0 : arena.name;
        arenaConverted.adress = arena === null || arena === void 0 ? void 0 : arena.adress;
        arenaConverted.district = arena === null || arena === void 0 ? void 0 : arena.district;
        arenaConverted.endHour = arena === null || arena === void 0 ? void 0 : arena.endHour;
        arenaConverted.longitude = arena === null || arena === void 0 ? void 0 : arena.longitude;
        arenaConverted.latitude = arena === null || arena === void 0 ? void 0 : arena.latitude;
        arenaConverted.isOpen = arena === null || arena === void 0 ? void 0 : arena.isOpen;
        arenaConverted.maxHours = arena === null || arena === void 0 ? void 0 : arena.maxHours;
        arenaConverted.startHour = arena === null || arena === void 0 ? void 0 : arena.startHour;
        arenaConverted.contacts = [];
        arenaConverted.pictures = arena_files_mapper_1.ArenaFilesMapper.convertFileEntityDtoToArenaFiles(arena === null || arena === void 0 ? void 0 : arena.pictures);
        (_a = arena.contacts) === null || _a === void 0 ? void 0 : _a.forEach(contact => arenaConverted.contacts.push(new contact_entity_1.Contact(contact)));
        return arenaConverted;
    }
    static convertToGetArenaDto(arena) {
        var _a;
        if (!arena)
            return null;
        const arenaToGet = new get_dto_2.GetArenaDto();
        arenaToGet.contacts = [];
        arenaToGet.code = arena === null || arena === void 0 ? void 0 : arena.pk;
        arenaToGet.name = arena === null || arena === void 0 ? void 0 : arena.name;
        arenaToGet.adress = arena === null || arena === void 0 ? void 0 : arena.adress;
        arenaToGet.district = arena === null || arena === void 0 ? void 0 : arena.district;
        arenaToGet.endHour = arena === null || arena === void 0 ? void 0 : arena.endHour;
        arenaToGet.longitude = arena === null || arena === void 0 ? void 0 : arena.longitude;
        arenaToGet.latitude = arena === null || arena === void 0 ? void 0 : arena.latitude;
        arenaToGet.isOpen = arena === null || arena === void 0 ? void 0 : arena.isOpen;
        arenaToGet.maxHours = arena === null || arena === void 0 ? void 0 : arena.maxHours;
        arenaToGet.startHour = arena === null || arena === void 0 ? void 0 : arena.startHour;
        arenaToGet.pictures = convert_file_entity_mapper_1.ConvertFileEntityMapper.toDtoList(arena.pictures);
        (_a = arena === null || arena === void 0 ? void 0 : arena.contacts) === null || _a === void 0 ? void 0 : _a.forEach(contact => arenaToGet.contacts.push(new get_dto_1.GetContactDto(contact)));
        return arenaToGet;
    }
    static convertUpdateArenaDtoToArena(arena, arenaToUpdate) {
        if (!arena)
            return null;
        if (arena.adress) {
            arenaToUpdate.adress = arena.adress;
        }
        if (arena.longitude) {
            arenaToUpdate.longitude = arena.longitude;
        }
        if (arena.latitude) {
            arenaToUpdate.latitude = arena.latitude;
        }
        if (arena.name) {
            arenaToUpdate.name = arena.name;
        }
        if (arena.startHour) {
            arenaToUpdate.startHour = arena.startHour;
        }
        if (arena.maxHours) {
            arenaToUpdate.maxHours = arena.maxHours;
        }
        if (arena.isOpen) {
            arenaToUpdate.isOpen = arena.isOpen;
        }
        if (arena.district) {
            arenaToUpdate.district = arena.district;
        }
        if (arena.isSubscribed) {
            arenaToUpdate.isSubscribed = arena.isSubscribed;
        }
        if (arena.endHour) {
            arenaToUpdate.endHour = arena.endHour;
        }
        if (arena.pictures) {
            arenaToUpdate.pictures = arena.pictures;
        }
        return arenaToUpdate;
    }
}
exports.ConvertArenaMapper = ConvertArenaMapper;
//# sourceMappingURL=convert-arena.mapper.js.map