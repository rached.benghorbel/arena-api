import { FileEntityDto } from "src/communs/dto/file-entity.dto";
import { ArenaFiles } from "../entities/arena-files.entity";
export declare abstract class ArenaFilesMapper {
    static toArenaFiles(file: FileEntityDto): ArenaFiles;
    static convertFileEntityDtoToArenaFiles(files: Array<FileEntityDto>): Array<ArenaFiles>;
}
