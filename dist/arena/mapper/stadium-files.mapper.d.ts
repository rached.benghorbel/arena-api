import { FileEntityDto } from "src/communs/dto/file-entity.dto";
import { StadiumFiles } from "../entities/stadium-files.entity";
export declare abstract class StadiumFilesMapper {
    static toArenaFiles(file: FileEntityDto): StadiumFiles;
    static convertFileEntityDtoToArenaFiles(files: Array<FileEntityDto>): Array<StadiumFiles>;
}
