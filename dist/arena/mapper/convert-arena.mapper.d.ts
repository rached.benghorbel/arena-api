import { CreateArenaDto } from '../dto/arena/create.dto';
import { GetArenaDto } from '../dto/arena/get.dto';
import { UpdateArenaDto } from '../dto/arena/update.dto';
import { Arena } from '../entities/arena.entity';
export declare class ConvertArenaMapper {
    static convertToArena(arena?: CreateArenaDto | GetArenaDto): Arena;
    static convertToGetArenaDto(arena?: Arena): GetArenaDto;
    static convertUpdateArenaDtoToArena(arena?: UpdateArenaDto, arenaToUpdate?: Arena): Arena;
}
