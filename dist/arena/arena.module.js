"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArenaModule = void 0;
const common_1 = require("@nestjs/common");
const auth_module_1 = require("../auth/auth.module");
const base_dao_1 = require("../communs/dao/base.dao");
const arena_controller_1 = require("./controllers/arena/arena.controller");
const stadium_controller_1 = require("./controllers/stadium/stadium.controller");
const arena_service_1 = require("./services/arena/arena.service");
const stadium_dao_1 = require("./dao/stadium.dao");
const stadium_service_1 = require("./services/stadium/stadium.service");
const reservation_controller_1 = require("./controllers/reservation/reservation.controller");
const reservation_service_1 = require("./services/reservation/reservation.service");
const reservation_dao_1 = require("./dao/reservation.dao");
let ArenaModule = class ArenaModule {
};
ArenaModule = __decorate([
    common_1.Module({
        imports: [
            auth_module_1.AuthModule
        ],
        controllers: [
            reservation_controller_1.ReservationController,
            stadium_controller_1.StadiumController,
            arena_controller_1.ArenaController
        ],
        providers: [
            stadium_service_1.StadiumService,
            arena_service_1.ArenaService,
            reservation_service_1.ReservationService,
            base_dao_1.BaseDAO,
            stadium_dao_1.StadiumDao,
            reservation_dao_1.ReservationDao
        ]
    })
], ArenaModule);
exports.ArenaModule = ArenaModule;
//# sourceMappingURL=arena.module.js.map