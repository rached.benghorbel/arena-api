import { CreateArenaDto } from "src/arena/dto/arena/create.dto";
import { GetArenaDto } from "src/arena/dto/arena/get.dto";
import { UpdateArenaDto } from "src/arena/dto/arena/update.dto";
import { Arena } from "src/arena/entities/arena.entity";
export interface IArenaService {
    createArena(arena: CreateArenaDto): Promise<GetArenaDto>;
    getArenaDetail(pk: string): Promise<GetArenaDto>;
    listArena(): Promise<Array<GetArenaDto>>;
    updateArena(arena: UpdateArenaDto): Promise<GetArenaDto>;
    findArenaByUser(userId: string): Promise<Arena>;
}
