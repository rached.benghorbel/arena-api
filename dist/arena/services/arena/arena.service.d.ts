import { CreateArenaDto } from 'src/arena/dto/arena/create.dto';
import { GetArenaDto } from 'src/arena/dto/arena/get.dto';
import { UpdateArenaDto } from 'src/arena/dto/arena/update.dto';
import { Arena } from 'src/arena/entities/arena.entity';
import { BaseDAO } from 'src/communs/dao/base.dao';
import { IArenaService } from './arena.service.interface';
export declare class ArenaService implements IArenaService {
    private readonly dao;
    private arenaBaseParms;
    constructor(dao: BaseDAO<Arena>);
    findArenaByUser(userId: string): Promise<Arena>;
    createArena(arena: CreateArenaDto): Promise<GetArenaDto>;
    getArenaDetail(code: string): Promise<GetArenaDto>;
    listArena(): Promise<GetArenaDto[]>;
    updateArena(arena: UpdateArenaDto): Promise<GetArenaDto>;
}
