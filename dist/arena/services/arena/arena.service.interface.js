"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const create_dto_1 = require("../../dto/arena/create.dto");
const get_dto_1 = require("../../dto/arena/get.dto");
const update_dto_1 = require("../../dto/arena/update.dto");
const arena_entity_1 = require("../../entities/arena.entity");
//# sourceMappingURL=arena.service.interface.js.map