"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArenaService = void 0;
const common_1 = require("@nestjs/common");
const create_dto_1 = require("../../dto/arena/create.dto");
const get_dto_1 = require("../../dto/arena/get.dto");
const update_dto_1 = require("../../dto/arena/update.dto");
const arena_entity_1 = require("../../entities/arena.entity");
const convert_arena_mapper_1 = require("../../mapper/convert-arena.mapper");
const base_dao_1 = require("../../../communs/dao/base.dao");
const base_params_1 = require("../../../communs/entities/base-params");
const helper_1 = require("../../../communs/helpers/helper");
let ArenaService = class ArenaService {
    constructor(dao) {
        this.dao = dao;
        this.arenaBaseParms = new base_params_1.BaseParams();
        this.arenaBaseParms.currentRepository = 'Arena';
        this.arenaBaseParms.relations = ['contacts'];
        this.dao.setBaseParams(this.arenaBaseParms);
    }
    findArenaByUser(userId) {
        return this.dao.findOne({ 'createdU': userId });
    }
    async createArena(arena) {
        if (await this.findArenaByUser(helper_1.Helper.currentUser.pk)) {
            throw new common_1.HttpException('User already have arena', common_1.HttpStatus.FORBIDDEN);
        }
        return convert_arena_mapper_1.ConvertArenaMapper.convertToGetArenaDto(await this.dao.add(convert_arena_mapper_1.ConvertArenaMapper.convertToArena(arena)));
    }
    async getArenaDetail(code) {
        const arena = convert_arena_mapper_1.ConvertArenaMapper.convertToGetArenaDto(await this.dao.findOne({ pk: code }, true));
        if (!arena) {
            throw new common_1.HttpException('The requested arena was not found', common_1.HttpStatus.NOT_FOUND);
        }
        return arena;
    }
    async listArena() {
        const arenaList = await this.dao.findAll(true);
        const arenaListToGet = [];
        arenaList.forEach(arena => arenaListToGet.push(convert_arena_mapper_1.ConvertArenaMapper.convertToGetArenaDto(arena)));
        return arenaListToGet;
    }
    async updateArena(arena) {
        let arenaToUpdate = await this.dao.findOne({ pk: arena.code }, true);
        if (arenaToUpdate) {
            arenaToUpdate = convert_arena_mapper_1.ConvertArenaMapper.convertUpdateArenaDtoToArena(arena, arenaToUpdate);
        }
        else {
            throw new common_1.HttpException('The requested arena was not found', common_1.HttpStatus.NOT_FOUND);
        }
        return convert_arena_mapper_1.ConvertArenaMapper.convertToGetArenaDto(await this.dao.update(arenaToUpdate, arena.code));
    }
};
ArenaService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [base_dao_1.BaseDAO])
], ArenaService);
exports.ArenaService = ArenaService;
//# sourceMappingURL=arena.service.js.map