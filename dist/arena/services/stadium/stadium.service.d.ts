import { CreateStadiumDto } from 'src/arena/dto/stadium/create.dto';
import { GetStadiumDto } from 'src/arena/dto/stadium/get.dto';
import { UpdateStadiumDto } from 'src/arena/dto/stadium/update.dto';
import { StadiumDao } from '../../dao/stadium.dao';
import { IStadiumService } from './stadium.service.interface';
export declare class StadiumService implements IStadiumService {
    private readonly dao;
    private stadiumBaseParams;
    constructor(dao: StadiumDao);
    createStadium(stadium: CreateStadiumDto): Promise<GetStadiumDto>;
    getAllStadiums(): Promise<GetStadiumDto[]>;
    getStadium(stadiumId: string): Promise<GetStadiumDto>;
    getStadiumByArena(arenaId: string): Promise<Array<GetStadiumDto>>;
    updateStadium(stadium: UpdateStadiumDto): Promise<GetStadiumDto>;
}
