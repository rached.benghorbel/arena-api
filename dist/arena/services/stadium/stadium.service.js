"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StadiumService = void 0;
const common_1 = require("@nestjs/common");
const create_dto_1 = require("../../dto/stadium/create.dto");
const get_dto_1 = require("../../dto/stadium/get.dto");
const update_dto_1 = require("../../dto/stadium/update.dto");
const arena_entity_1 = require("../../entities/arena.entity");
const stadium_entity_1 = require("../../entities/stadium.entity");
const convert_stadium_mapper_1 = require("../../mapper/convert-stadium.mapper");
const base_params_1 = require("../../../communs/entities/base-params");
const stadium_dao_1 = require("../../dao/stadium.dao");
let StadiumService = class StadiumService {
    constructor(dao) {
        this.dao = dao;
        this.stadiumBaseParams = new base_params_1.BaseParams();
        this.stadiumBaseParams.currentRepository = 'Stadium';
        this.stadiumBaseParams.relations = ["arena"];
        this.dao.setBaseParams(this.stadiumBaseParams);
    }
    async createStadium(stadium) {
        return convert_stadium_mapper_1.ConvertStadiumMapper.convertToGetStadiumDto(await this.dao.add(convert_stadium_mapper_1.ConvertStadiumMapper.convertToStadium(stadium)));
    }
    async getAllStadiums() {
        const stadiumList = [];
        const foundStadiums = await this.dao.findAll();
        foundStadiums.forEach(stadium => stadiumList.push(convert_stadium_mapper_1.ConvertStadiumMapper.convertToGetStadiumDto(stadium)));
        return stadiumList;
    }
    async getStadium(stadiumId) {
        const stadium = await this.dao.findOne({ pk: stadiumId });
        if (!stadium) {
            throw new common_1.HttpException('the requested stadium is not found', common_1.HttpStatus.NOT_FOUND);
        }
        return convert_stadium_mapper_1.ConvertStadiumMapper.convertToGetStadiumDto(stadium);
    }
    async getStadiumByArena(arenaId) {
        const arenaToFind = new arena_entity_1.Arena();
        arenaToFind.pk = arenaId;
        const stadiums = await this.dao.findByParams({ arena: arenaToFind });
        if (!stadiums) {
            throw new common_1.HttpException('the requested arena has no stadiums', common_1.HttpStatus.NOT_FOUND);
        }
        return convert_stadium_mapper_1.ConvertStadiumMapper.convertListOfStadiumsToGetStadiumDto(stadiums);
    }
    async updateStadium(stadium) {
        let stadiumToUpdate = await this.dao.findOne({ pk: stadium.code });
        if (!stadiumToUpdate) {
            throw new common_1.HttpException('the requested stadium is not found', common_1.HttpStatus.NOT_FOUND);
        }
        stadiumToUpdate = await this.dao.update(convert_stadium_mapper_1.ConvertStadiumMapper.convertUpdateToStadium(stadium, stadiumToUpdate), stadiumToUpdate.pk);
        return convert_stadium_mapper_1.ConvertStadiumMapper.convertToGetStadiumDto(stadiumToUpdate);
    }
};
StadiumService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [stadium_dao_1.StadiumDao])
], StadiumService);
exports.StadiumService = StadiumService;
//# sourceMappingURL=stadium.service.js.map