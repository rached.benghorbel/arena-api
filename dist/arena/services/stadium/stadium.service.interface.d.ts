import { CreateStadiumDto } from "src/arena/dto/stadium/create.dto";
import { GetStadiumDto } from "src/arena/dto/stadium/get.dto";
import { UpdateStadiumDto } from "src/arena/dto/stadium/update.dto";
export interface IStadiumService {
    createStadium(stadium: CreateStadiumDto): Promise<GetStadiumDto>;
    getAllStadiums(): Promise<Array<GetStadiumDto>>;
    getStadium(stadiumId: string): Promise<GetStadiumDto>;
    getStadiumByArena(arenaId: string): Promise<Array<GetStadiumDto>>;
    updateStadium(stadium: UpdateStadiumDto): Promise<GetStadiumDto>;
}
