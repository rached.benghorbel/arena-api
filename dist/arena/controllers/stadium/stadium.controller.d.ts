import { CreateStadiumDto } from 'src/arena/dto/stadium/create.dto';
import { GetStadiumDto } from 'src/arena/dto/stadium/get.dto';
import { UpdateStadiumDto } from 'src/arena/dto/stadium/update.dto';
import { StadiumService } from 'src/arena/services/stadium/stadium.service';
export declare class StadiumController {
    private readonly stadiumService;
    constructor(stadiumService: StadiumService);
    createStadium(stadium: CreateStadiumDto): Promise<GetStadiumDto>;
    getAllStadiums(): Promise<Array<GetStadiumDto>>;
    getStadium(stadiumId: string): Promise<GetStadiumDto>;
    updateStadium(stadium: UpdateStadiumDto): Promise<GetStadiumDto>;
}
