"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StadiumController = void 0;
const common_1 = require("@nestjs/common");
const create_dto_1 = require("../../dto/stadium/create.dto");
const get_dto_1 = require("../../dto/stadium/get.dto");
const update_dto_1 = require("../../dto/stadium/update.dto");
const stadium_service_1 = require("../../services/stadium/stadium.service");
const jwt_auth_guard_1 = require("../../../auth/services/jwt-auth.guard");
let StadiumController = class StadiumController {
    constructor(stadiumService) {
        this.stadiumService = stadiumService;
    }
    async createStadium(stadium) {
        return this.stadiumService.createStadium(stadium);
    }
    async getAllStadiums() {
        return this.stadiumService.getAllStadiums();
    }
    async getStadium(stadiumId) {
        return this.stadiumService.getStadium(stadiumId);
    }
    async updateStadium(stadium) {
        return this.stadiumService.updateStadium(stadium);
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_dto_1.CreateStadiumDto]),
    __metadata("design:returntype", Promise)
], StadiumController.prototype, "createStadium", null);
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], StadiumController.prototype, "getAllStadiums", null);
__decorate([
    common_1.Get('/:stadiumId'),
    __param(0, common_1.Param('stadiumId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], StadiumController.prototype, "getStadium", null);
__decorate([
    common_1.Put(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_dto_1.UpdateStadiumDto]),
    __metadata("design:returntype", Promise)
], StadiumController.prototype, "updateStadium", null);
StadiumController = __decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Controller('stadium'),
    __metadata("design:paramtypes", [stadium_service_1.StadiumService])
], StadiumController);
exports.StadiumController = StadiumController;
//# sourceMappingURL=stadium.controller.js.map