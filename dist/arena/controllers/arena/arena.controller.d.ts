import { CreateArenaDto } from 'src/arena/dto/arena/create.dto';
import { GetArenaDto } from 'src/arena/dto/arena/get.dto';
import { UpdateArenaDto } from 'src/arena/dto/arena/update.dto';
import { GetStadiumDto } from 'src/arena/dto/stadium/get.dto';
import { ArenaService } from 'src/arena/services/arena/arena.service';
import { StadiumService } from 'src/arena/services/stadium/stadium.service';
export declare class ArenaController {
    private readonly arenaService;
    private readonly stadiumService;
    constructor(arenaService: ArenaService, stadiumService: StadiumService);
    createArena(arena: CreateArenaDto): Promise<GetArenaDto>;
    getArenaList(): Promise<Array<GetArenaDto>>;
    getArena(code: string): Promise<GetArenaDto>;
    getStadiumsByArena(arenaId: string): Promise<Array<GetStadiumDto>>;
    updateArena(arena: UpdateArenaDto): Promise<GetArenaDto>;
}
