"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArenaController = void 0;
const common_1 = require("@nestjs/common");
const create_dto_1 = require("../../dto/arena/create.dto");
const get_dto_1 = require("../../dto/arena/get.dto");
const update_dto_1 = require("../../dto/arena/update.dto");
const get_dto_2 = require("../../dto/stadium/get.dto");
const arena_service_1 = require("../../services/arena/arena.service");
const stadium_service_1 = require("../../services/stadium/stadium.service");
const jwt_auth_guard_1 = require("../../../auth/services/jwt-auth.guard");
let ArenaController = class ArenaController {
    constructor(arenaService, stadiumService) {
        this.arenaService = arenaService;
        this.stadiumService = stadiumService;
    }
    ;
    async createArena(arena) {
        return await this.arenaService.createArena(arena);
    }
    async getArenaList() {
        return await this.arenaService.listArena();
    }
    async getArena(code) {
        return await this.arenaService.getArenaDetail(code);
    }
    async getStadiumsByArena(arenaId) {
        return this.stadiumService.getStadiumByArena(arenaId);
    }
    async updateArena(arena) {
        return await this.arenaService.updateArena(arena);
    }
};
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_dto_1.CreateArenaDto]),
    __metadata("design:returntype", Promise)
], ArenaController.prototype, "createArena", null);
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ArenaController.prototype, "getArenaList", null);
__decorate([
    common_1.Get('/:arenaCode'),
    __param(0, common_1.Param('arenaCode')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ArenaController.prototype, "getArena", null);
__decorate([
    common_1.Get('/stadiums/:arenaId'),
    __param(0, common_1.Param('arenaId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ArenaController.prototype, "getStadiumsByArena", null);
__decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Put(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_dto_1.UpdateArenaDto]),
    __metadata("design:returntype", Promise)
], ArenaController.prototype, "updateArena", null);
ArenaController = __decorate([
    common_1.Controller('arena'),
    __metadata("design:paramtypes", [arena_service_1.ArenaService,
        stadium_service_1.StadiumService])
], ArenaController);
exports.ArenaController = ArenaController;
//# sourceMappingURL=arena.controller.js.map