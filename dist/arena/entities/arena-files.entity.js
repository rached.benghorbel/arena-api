"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArenaFiles = void 0;
const file_entity_1 = require("../../communs/entities/file-entity");
const typeorm_1 = require("typeorm");
const arena_entity_1 = require("./arena.entity");
let ArenaFiles = class ArenaFiles extends file_entity_1.FileEntity {
};
__decorate([
    typeorm_1.ManyToOne(type => arena_entity_1.Arena, arena => arena.pictures),
    __metadata("design:type", arena_entity_1.Arena)
], ArenaFiles.prototype, "arena", void 0);
ArenaFiles = __decorate([
    typeorm_1.Entity()
], ArenaFiles);
exports.ArenaFiles = ArenaFiles;
//# sourceMappingURL=arena-files.entity.js.map