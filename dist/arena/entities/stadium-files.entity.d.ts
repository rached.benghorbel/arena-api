import { FileEntity } from "src/communs/entities/file-entity";
import { Stadium } from "./stadium.entity";
export declare class StadiumFiles extends FileEntity {
    stadium: Stadium;
}
