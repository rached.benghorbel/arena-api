"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MaxHours = void 0;
var MaxHours;
(function (MaxHours) {
    MaxHours["ONE_HOUR"] = "ONE_HOUR";
    MaxHours["ONE_AND_HAL_HOUR"] = "ONE_AND_HALF_HOUR";
    MaxHours["TWO_HOURS"] = "TWO_HOURS";
})(MaxHours = exports.MaxHours || (exports.MaxHours = {}));
//# sourceMappingURL=max-hours.enum.js.map