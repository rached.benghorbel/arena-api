import { BaseEntity } from "src/communs/entities/base-entity";
import { Arena } from "./arena.entity";
import { Reservation } from "./reservation.entity";
import { StadiumFiles } from "./stadium-files.entity";
export declare class Stadium extends BaseEntity {
    name?: string;
    description?: string;
    playerNumber: number;
    isAvailable?: boolean;
    arena: Arena;
    reservations: Array<Reservation>;
    pictures: Array<StadiumFiles>;
}
