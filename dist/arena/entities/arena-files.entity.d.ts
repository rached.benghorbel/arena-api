import { FileEntity } from "src/communs/entities/file-entity";
import { Arena } from "./arena.entity";
export declare class ArenaFiles extends FileEntity {
    arena: Arena;
}
