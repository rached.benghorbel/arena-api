"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Stadium = void 0;
const base_entity_1 = require("../../communs/entities/base-entity");
const typeorm_1 = require("typeorm");
const arena_entity_1 = require("./arena.entity");
const reservation_entity_1 = require("./reservation.entity");
const stadium_files_entity_1 = require("./stadium-files.entity");
let Stadium = class Stadium extends base_entity_1.BaseEntity {
    constructor() {
        super(...arguments);
        this.isAvailable = true;
    }
};
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Stadium.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Stadium.prototype, "description", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], Stadium.prototype, "playerNumber", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Boolean)
], Stadium.prototype, "isAvailable", void 0);
__decorate([
    typeorm_1.ManyToOne(() => arena_entity_1.Arena, arena => arena.stadiums),
    __metadata("design:type", arena_entity_1.Arena)
], Stadium.prototype, "arena", void 0);
__decorate([
    typeorm_1.OneToMany(() => reservation_entity_1.Reservation, reservation => reservation.reservedStadium),
    __metadata("design:type", Array)
], Stadium.prototype, "reservations", void 0);
__decorate([
    typeorm_1.OneToMany(() => stadium_files_entity_1.StadiumFiles, photo => photo.stadium, {
        eager: true,
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    }),
    typeorm_1.JoinTable(),
    __metadata("design:type", Array)
], Stadium.prototype, "pictures", void 0);
Stadium = __decorate([
    typeorm_1.Entity()
], Stadium);
exports.Stadium = Stadium;
//# sourceMappingURL=stadium.entity.js.map