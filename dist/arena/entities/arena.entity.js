"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Arena = void 0;
const base_entity_1 = require("../../communs/entities/base-entity");
const create_dto_1 = require("../../users/dto/contact/create.dto");
const contact_entity_1 = require("../../users/entities/contact.entity");
const typeorm_1 = require("typeorm");
const create_dto_2 = require("../dto/arena/create.dto");
const arena_files_entity_1 = require("./arena-files.entity");
const max_hours_enum_1 = require("./max-hours.enum");
const stadium_entity_1 = require("./stadium.entity");
let Arena = class Arena extends base_entity_1.BaseEntity {
    constructor(arena) {
        super();
        this.name = arena === null || arena === void 0 ? void 0 : arena.name;
        this.adress = arena === null || arena === void 0 ? void 0 : arena.adress;
        this.district = arena === null || arena === void 0 ? void 0 : arena.district;
        this.endHour = arena === null || arena === void 0 ? void 0 : arena.endHour;
        this.longitude = arena === null || arena === void 0 ? void 0 : arena.longitude;
        this.latitude = arena === null || arena === void 0 ? void 0 : arena.latitude;
        this.isOpen = arena === null || arena === void 0 ? void 0 : arena.isOpen;
        this.maxHours = arena === null || arena === void 0 ? void 0 : arena.maxHours;
        this.startHour = arena === null || arena === void 0 ? void 0 : arena.startHour;
        this.contacts = this.convertToContactsObject(arena === null || arena === void 0 ? void 0 : arena.contacts);
    }
    convertToContactsObject(contacts) {
        if (!contacts)
            return null;
        let convertedContacts;
        contacts.forEach(contact => convertedContacts.push(new contact_entity_1.Contact(contact)));
        return convertedContacts;
    }
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Arena.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: "float", nullable: true }),
    __metadata("design:type", Number)
], Arena.prototype, "longitude", void 0);
__decorate([
    typeorm_1.Column({ type: "float", nullable: true }),
    __metadata("design:type", Number)
], Arena.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({ type: "timestamp" }),
    __metadata("design:type", Date)
], Arena.prototype, "startHour", void 0);
__decorate([
    typeorm_1.Column({ type: "timestamp" }),
    __metadata("design:type", Date)
], Arena.prototype, "endHour", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Arena.prototype, "adress", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Arena.prototype, "district", void 0);
__decorate([
    typeorm_1.Column({ default: true }),
    __metadata("design:type", Boolean)
], Arena.prototype, "isOpen", void 0);
__decorate([
    typeorm_1.Column({ default: true }),
    __metadata("design:type", Boolean)
], Arena.prototype, "isSubscribed", void 0);
__decorate([
    typeorm_1.Column({ default: max_hours_enum_1.MaxHours.ONE_HOUR }),
    __metadata("design:type", String)
], Arena.prototype, "maxHours", void 0);
__decorate([
    typeorm_1.OneToMany(() => contact_entity_1.Contact, contact => contact.arena, {
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    }),
    __metadata("design:type", Array)
], Arena.prototype, "contacts", void 0);
__decorate([
    typeorm_1.OneToMany(() => stadium_entity_1.Stadium, stadium => stadium.arena),
    __metadata("design:type", Array)
], Arena.prototype, "stadiums", void 0);
__decorate([
    typeorm_1.OneToMany(type => arena_files_entity_1.ArenaFiles, photo => photo.arena, {
        eager: true,
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    }),
    typeorm_1.JoinTable(),
    __metadata("design:type", Array)
], Arena.prototype, "pictures", void 0);
Arena = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [create_dto_2.CreateArenaDto])
], Arena);
exports.Arena = Arena;
//# sourceMappingURL=arena.entity.js.map