import { BaseEntity } from "src/communs/entities/base-entity";
import { Contact } from "src/users/entities/contact.entity";
import { CreateArenaDto } from "../dto/arena/create.dto";
import { ArenaFiles } from "./arena-files.entity";
import { MaxHours } from "./max-hours.enum";
import { Stadium } from "./stadium.entity";
export declare class Arena extends BaseEntity {
    name: string;
    longitude?: number;
    latitude?: number;
    startHour: Date;
    endHour: Date;
    adress: string;
    district?: string;
    isOpen?: boolean;
    isSubscribed?: boolean;
    maxHours?: MaxHours;
    contacts: Array<Contact>;
    stadiums: Array<Stadium>;
    pictures: ArenaFiles[];
    constructor();
    constructor(arena: CreateArenaDto);
    private convertToContactsObject;
}
