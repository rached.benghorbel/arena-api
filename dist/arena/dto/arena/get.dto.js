"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetArenaDto = void 0;
const arena_entity_1 = require("../../entities/arena.entity");
const max_hours_enum_1 = require("../../entities/max-hours.enum");
const file_entity_dto_1 = require("../../../communs/dto/file-entity.dto");
const get_dto_1 = require("../../../users/dto/contact/get.dto");
class GetArenaDto {
    constructor(arena) {
        this.code = arena === null || arena === void 0 ? void 0 : arena.pk;
        this.name = arena === null || arena === void 0 ? void 0 : arena.name;
        this.adress = arena === null || arena === void 0 ? void 0 : arena.adress;
        this.contacts = arena === null || arena === void 0 ? void 0 : arena.contacts;
        this.district = arena === null || arena === void 0 ? void 0 : arena.district;
        this.endHour = arena === null || arena === void 0 ? void 0 : arena.endHour;
        this.longitude = arena === null || arena === void 0 ? void 0 : arena.longitude;
        this.latitude = arena === null || arena === void 0 ? void 0 : arena.latitude;
        this.isOpen = arena === null || arena === void 0 ? void 0 : arena.isOpen;
        this.maxHours = arena === null || arena === void 0 ? void 0 : arena.maxHours;
        this.startHour = arena === null || arena === void 0 ? void 0 : arena.startHour;
    }
}
exports.GetArenaDto = GetArenaDto;
//# sourceMappingURL=get.dto.js.map