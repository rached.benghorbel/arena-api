"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateArenaDto = void 0;
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const arena_files_entity_1 = require("../../entities/arena-files.entity");
const max_hours_enum_1 = require("../../entities/max-hours.enum");
const create_dto_1 = require("../../../users/dto/contact/create.dto");
class UpdateArenaDto {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    __metadata("design:type", String)
], UpdateArenaDto.prototype, "code", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateArenaDto.prototype, "name", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UpdateArenaDto.prototype, "longitude", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Number)
], UpdateArenaDto.prototype, "latitude", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Date)
], UpdateArenaDto.prototype, "startHour", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Date)
], UpdateArenaDto.prototype, "endHour", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateArenaDto.prototype, "adress", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateArenaDto.prototype, "district", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Boolean)
], UpdateArenaDto.prototype, "isOpen", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", Boolean)
], UpdateArenaDto.prototype, "isSubscribed", void 0);
__decorate([
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UpdateArenaDto.prototype, "maxHours", void 0);
__decorate([
    class_validator_1.ValidateNested({ each: true }),
    class_transformer_1.Type(() => create_dto_1.CreateContactDto),
    class_validator_1.IsArray(),
    class_validator_1.IsOptional(),
    __metadata("design:type", Array)
], UpdateArenaDto.prototype, "contacts", void 0);
__decorate([
    class_validator_1.IsArray(),
    __metadata("design:type", Array)
], UpdateArenaDto.prototype, "pictures", void 0);
exports.UpdateArenaDto = UpdateArenaDto;
//# sourceMappingURL=update.dto.js.map