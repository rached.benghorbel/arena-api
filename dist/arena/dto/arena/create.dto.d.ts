import { MaxHours } from "src/arena/entities/max-hours.enum";
import { CreateContactDto } from "src/users/dto/contact/create.dto";
import { ArenaFiles } from "src/arena/entities/arena-files.entity";
export declare class CreateArenaDto {
    name: string;
    longitude?: number;
    latitude?: number;
    startHour: Date;
    endHour: Date;
    adress: string;
    district?: string;
    isOpen?: boolean;
    isSubscribed: boolean;
    maxHours: MaxHours;
    contacts: Array<CreateContactDto>;
    pictures: Array<ArenaFiles>;
}
