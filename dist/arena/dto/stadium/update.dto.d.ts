import { FileEntityDto } from "src/communs/dto/file-entity.dto";
export declare class UpdateStadiumDto {
    code: string;
    name?: string;
    description: string;
    playerNumber: number;
    isAvailable: boolean;
    pictures: Array<FileEntityDto>;
}
