import { FileEntityDto } from "src/communs/dto/file-entity.dto";
import { GetArenaDto } from "../arena/get.dto";
export declare class CreateStadiumDto {
    name?: string;
    description?: string;
    playerNumber: number;
    isAvailable: boolean;
    arena: GetArenaDto;
    pictures: Array<FileEntityDto>;
}
