export declare class Mail {
    mailTo: string;
    mailFrom: string;
    mailSubject: string;
    mailTemplate: string;
    mailContext: any;
}
