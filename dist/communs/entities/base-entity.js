"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseEntity = void 0;
const entity_status_enum_1 = require("./entity-status.enum");
const typeorm_1 = require("typeorm");
const helper_1 = require("../helpers/helper");
class BaseEntity {
    saveIdentifiers() {
        var _a;
        this.created = new Date();
        this.entityStatus = entity_status_enum_1.EntityStatus.CREATED;
        this.createdU = (_a = helper_1.Helper.currentUser) === null || _a === void 0 ? void 0 : _a.pk;
    }
    updateIdentifiers() {
        var _a;
        this.updated = new Date();
        this.entityStatus = entity_status_enum_1.EntityStatus.UPDATED;
        this.updatedU = (_a = helper_1.Helper.currentUser) === null || _a === void 0 ? void 0 : _a.pk;
    }
}
__decorate([
    typeorm_1.PrimaryGeneratedColumn("uuid"),
    __metadata("design:type", String)
], BaseEntity.prototype, "pk", void 0);
__decorate([
    typeorm_1.Column(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Date)
], BaseEntity.prototype, "created", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Date)
], BaseEntity.prototype, "updated", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Date)
], BaseEntity.prototype, "deleted", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], BaseEntity.prototype, "createdU", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], BaseEntity.prototype, "updatedU", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], BaseEntity.prototype, "deletedU", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], BaseEntity.prototype, "entityStatus", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], BaseEntity.prototype, "saveIdentifiers", null);
__decorate([
    typeorm_1.BeforeUpdate(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], BaseEntity.prototype, "updateIdentifiers", null);
exports.BaseEntity = BaseEntity;
//# sourceMappingURL=base-entity.js.map