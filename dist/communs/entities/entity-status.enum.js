"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityStatus = void 0;
var EntityStatus;
(function (EntityStatus) {
    EntityStatus["CREATED"] = "CREATED";
    EntityStatus["UPDATED"] = "UPDATED";
    EntityStatus["DELETED"] = "DELETED";
})(EntityStatus = exports.EntityStatus || (exports.EntityStatus = {}));
//# sourceMappingURL=entity-status.enum.js.map