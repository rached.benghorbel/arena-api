import { BaseEntity } from "./base-entity";
export declare class FileEntity extends BaseEntity {
    name?: string;
    size?: number;
    extension?: string;
    path?: string;
    content?: any;
}
