import { IBaseEntity } from "../interfaces/ibase-entity.interface";
import { EntityStatus } from "./entity-status.enum";
export declare class BaseEntity implements IBaseEntity {
    pk: string;
    created: Date;
    updated?: Date;
    deleted?: Date;
    createdU: string;
    updatedU?: string;
    deletedU?: string;
    entityStatus: EntityStatus;
    saveIdentifiers(): void;
    updateIdentifiers(): void;
}
