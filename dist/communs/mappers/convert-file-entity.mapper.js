"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertFileEntityMapper = void 0;
const file_entity_dto_1 = require("../dto/file-entity.dto");
class ConvertFileEntityMapper {
    static toDto(file) {
        const fileEntityDto = new file_entity_dto_1.FileEntityDto();
        fileEntityDto.pk = file.pk;
        fileEntityDto.name = file.name;
        fileEntityDto.extension = file.extension;
        fileEntityDto.path = file.path;
        fileEntityDto.size = file.size;
        fileEntityDto.content = file.content;
        return fileEntityDto;
    }
    static toDtoList(files) {
        const filesEntityDto = [];
        files === null || files === void 0 ? void 0 : files.forEach(file => {
            filesEntityDto.push(this.toDto(file));
        });
        return filesEntityDto;
    }
}
exports.ConvertFileEntityMapper = ConvertFileEntityMapper;
//# sourceMappingURL=convert-file-entity.mapper.js.map