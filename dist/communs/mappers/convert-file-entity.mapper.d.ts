import { FileEntityDto } from "../dto/file-entity.dto";
import { FileEntity } from "../entities/file-entity";
export declare abstract class ConvertFileEntityMapper {
    static toDto(file: FileEntity): FileEntityDto;
    static toDtoList(files?: Array<FileEntity>): Array<FileEntityDto>;
}
