import { EntityStatus } from "../entities/entity-status.enum";
export interface IBaseEntity {
    pk: string;
    created: Date;
    updated?: Date;
    deleted?: Date;
    createdU: string;
    updatedU?: string;
    deletedU?: string;
    entityStatus: EntityStatus;
}
