export interface IBaseDAO<T> {
    add(entity: T): Promise<T>;
    update(entity: T, id: string): Promise<T>;
    delete(id: string): Promise<any>;
    findOne(params: any): Promise<T>;
    findByParams(params: T, includeRelations: boolean): Promise<T[]>;
    findAll(): Promise<T[]>;
}
