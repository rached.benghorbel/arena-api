import { MailerService } from '@nestjs-modules/mailer';
import { Mail } from 'src/communs/entities/mail-entity';
export declare class MailService {
    private readonly mailerService;
    constructor(mailerService: MailerService);
    sendMail(mailToSend: Mail): void;
}
