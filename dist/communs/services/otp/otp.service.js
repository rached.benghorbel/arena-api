"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OtpService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const otplib_1 = require("otplib");
const app_config_1 = require("../../../app.config");
let OtpService = class OtpService {
    constructor(configService) {
        this.configService = configService;
    }
    generateOtp() {
        otplib_1.totp.options = {
            digits: app_config_1.AppConfig.otpDigits,
            epoch: Date.now(),
            step: app_config_1.AppConfig.otpStep,
            window: 0,
        };
        return otplib_1.totp.generate(this.configService.get('OTP_SECRET'));
    }
    checkOtp(otp) {
        return otplib_1.totp.check(otp, this.configService.get('OTP_SECRET'));
    }
};
OtpService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], OtpService);
exports.OtpService = OtpService;
//# sourceMappingURL=otp.service.js.map