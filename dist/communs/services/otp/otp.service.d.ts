import { ConfigService } from '@nestjs/config';
import { IOtpService } from './otp.service.interface';
export declare class OtpService implements IOtpService {
    private readonly configService;
    constructor(configService: ConfigService);
    generateOtp(): string;
    checkOtp(otp: string): boolean;
}
