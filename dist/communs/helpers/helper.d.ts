/// <reference types="node" />
import { Users } from "src/users/entities/Users.entity";
export declare class Helper {
    static currentUser: Users;
    static makeId(length: number): string;
    static convertBlobToBase64(content: any): string;
    static convertBase64ToBlob(content: any): Buffer;
}
