"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Helper = void 0;
const Users_entity_1 = require("../../users/entities/Users.entity");
class Helper {
    static makeId(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    static convertBlobToBase64(content) {
        return Buffer.from(content, 'binary').toString('base64');
    }
    static convertBase64ToBlob(content) {
        return Buffer.from(content, "base64");
    }
}
exports.Helper = Helper;
//# sourceMappingURL=helper.js.map