import { IBaseDAO } from "../interfaces/ibase-service.interface";
import { BaseParams } from "../entities/base-params";
import { BaseEntity } from "../entities/base-entity";
export declare class BaseDAO<T extends BaseEntity> implements IBaseDAO<T> {
    repository: any;
    private baseParams;
    setBaseParams(receivedPramas: BaseParams): void;
    add(entity: T): Promise<T>;
    update(entity: T, id: string): Promise<T>;
    delete(id: string): Promise<T>;
    findOne(params: any, includeRelations?: boolean): Promise<T>;
    findByParams(params: any, includeRelations?: boolean): Promise<T[]>;
    findAll(includeRelations?: boolean): Promise<T[]>;
}
