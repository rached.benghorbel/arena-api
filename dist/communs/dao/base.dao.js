"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDAO = void 0;
const typeorm_1 = require("typeorm");
const common_1 = require("@nestjs/common");
const entity_status_enum_1 = require("../entities/entity-status.enum");
let BaseDAO = class BaseDAO {
    setBaseParams(receivedPramas) {
        this.baseParams = receivedPramas;
    }
    async add(entity) {
        try {
            this.repository = typeorm_1.getRepository(this.baseParams.currentRepository);
            const newEntity = (this.repository.create(entity));
            await this.repository.save(newEntity);
            return newEntity;
        }
        catch (exception) {
            throw new common_1.HttpException(exception.toString(), common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async update(entity, id) {
        try {
            this.repository = typeorm_1.getRepository(this.baseParams.currentRepository);
            const updatedEntity = (entity);
            await this.repository.save(updatedEntity);
            return this.repository.findOne(id);
        }
        catch (exception) {
            throw new common_1.HttpException(exception.toString(), common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    delete(id) {
        throw new Error("Method not implemented.");
    }
    async findOne(params, includeRelations = false) {
        try {
            this.repository = typeorm_1.getRepository(this.baseParams.currentRepository);
            if (includeRelations)
                return await this.repository.findOne({
                    where: params,
                    andWhere: { entityStatus: typeorm_1.Not(entity_status_enum_1.EntityStatus.DELETED) },
                    relations: this.baseParams.relations
                });
            return await this.repository.findOne({ where: params });
        }
        catch (exception) {
            throw new common_1.HttpException(exception.toString(), common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async findByParams(params, includeRelations = false) {
        try {
            this.repository = typeorm_1.getRepository(this.baseParams.currentRepository);
            if (includeRelations)
                return await this.repository.find({
                    where: params,
                    andWhere: { entityStatus: typeorm_1.Not(entity_status_enum_1.EntityStatus.DELETED) },
                    relations: this.baseParams.relations
                });
            return await this.repository.find({
                where: params,
                andWhere: { entityStatus: typeorm_1.Not(entity_status_enum_1.EntityStatus.DELETED) }
            });
        }
        catch (exception) {
            throw new common_1.HttpException(exception.toString(), common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async findAll(includeRelations = false) {
        try {
            this.repository = typeorm_1.getRepository(this.baseParams.currentRepository);
            if (includeRelations)
                return await this.repository.find({
                    relations: this.baseParams.relations,
                    where: { entityStatus: typeorm_1.Not(entity_status_enum_1.EntityStatus.DELETED) }
                });
            return await this.repository.find();
        }
        catch (exception) {
            throw new common_1.HttpException(exception.toString(), common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
};
BaseDAO = __decorate([
    common_1.Injectable()
], BaseDAO);
exports.BaseDAO = BaseDAO;
//# sourceMappingURL=base.dao.js.map