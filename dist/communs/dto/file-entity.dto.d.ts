export declare class FileEntityDto {
    pk?: string;
    name?: string;
    size?: number;
    extension?: string;
    path?: string;
    content?: string;
}
