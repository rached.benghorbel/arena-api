"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const base_params_1 = require("../../communs/entities/base-params");
const common_1 = require("@nestjs/common");
const bcrypt = require("bcrypt");
const jwt_1 = require("@nestjs/jwt");
const get_dto_1 = require("../../users/dto/users/get.dto");
const base_dao_1 = require("../../communs/dao/base.dao");
const Users_entity_1 = require("../../users/entities/Users.entity");
const user_dao_1 = require("../../users/dao/user.dao");
const otp_service_1 = require("../../communs/services/otp/otp.service");
const users_service_1 = require("../../users/services/users.service");
const mail_service_1 = require("../../communs/services/mail/mail.service");
const mail_entity_1 = require("../../communs/entities/mail-entity");
const app_config_1 = require("../../app.config");
const helper_1 = require("../../communs/helpers/helper");
let AuthService = class AuthService {
    constructor(jwtService, dao, userDao, otpService, userService, mailService) {
        this.jwtService = jwtService;
        this.dao = dao;
        this.userDao = userDao;
        this.otpService = otpService;
        this.userService = userService;
        this.mailService = mailService;
        this.authParams = new base_params_1.BaseParams();
        this.authParams.currentRepository = 'Users';
        this.dao.setBaseParams(this.authParams);
    }
    async authenticate(login, password, otp) {
        const authenticatedUser = await this.userDao.findUserByEmail(login);
        if (!authenticatedUser) {
            throw new common_1.HttpException('email or phone number not found', common_1.HttpStatus.UNAUTHORIZED);
        }
        await this.verifyPassword(password, authenticatedUser.password);
        if (!authenticatedUser.isActive) {
            if (!this.checkOtp(authenticatedUser, otp) && otp) {
                throw new common_1.HttpException('Wrong given otp', common_1.HttpStatus.PRECONDITION_FAILED);
            }
            else if (!otp) {
                this.sendOtpVerification(authenticatedUser);
                throw new common_1.HttpException('Enter the password sent', common_1.HttpStatus.PRECONDITION_FAILED);
            }
        }
        return authenticatedUser;
    }
    async generateAccessToken(user) {
        const payload = { username: user.contact.email, sub: user.pk };
        return this.jwtService.sign(payload);
    }
    async checkUser(login, req) {
        try {
            const authenticatedUser = await this.userDao.findUserByEmail(login);
            if (authenticatedUser) {
                helper_1.Helper.currentUser = authenticatedUser;
                return authenticatedUser;
            }
        }
        catch (Exception) {
            throw new common_1.HttpException(Exception.message, common_1.HttpStatus.UNAUTHORIZED);
        }
    }
    async verifyPassword(plainTextPassword, hashedPassword) {
        const isPasswordMatching = await bcrypt.compare(plainTextPassword, hashedPassword);
        if (!isPasswordMatching) {
            throw new common_1.HttpException('Wrong credentials provided', common_1.HttpStatus.UNAUTHORIZED);
        }
    }
    checkOtp(authenticatedUser, otp) {
        if (!this.otpService.checkOtp(otp)) {
            return false;
        }
        this.userService.activateUser(authenticatedUser);
        return true;
    }
    sendOtpVerification(user) {
        const otp = this.otpService.generateOtp();
        const mailToSend = {
            mailTo: user.contact.email,
            mailContext: {
                appName: app_config_1.AppConfig.appName,
                firstName: user.firstName,
                lastName: user.lastName,
                otp: otp,
            },
            mailFrom: 'reserve arena',
            mailSubject: 'Confirmation code',
            mailTemplate: 'otp',
        };
        this.mailService.sendMail(mailToSend);
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        base_dao_1.BaseDAO,
        user_dao_1.UserDao,
        otp_service_1.OtpService,
        users_service_1.UserService,
        mail_service_1.MailService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map