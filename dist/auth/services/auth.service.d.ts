import { IAuthService } from '../interfaces/iauth-service.interface';
import { BaseParams } from 'src/communs/entities/base-params';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import { GetUserDto } from 'src/users/dto/users/get.dto';
import { BaseDAO } from 'src/communs/dao/base.dao';
import { Users } from 'src/users/entities/Users.entity';
import { UserDao } from 'src/users/dao/user.dao';
import { OtpService } from 'src/communs/services/otp/otp.service';
import { UserService } from 'src/users/services/users.service';
import { MailService } from 'src/communs/services/mail/mail.service';
export declare class AuthService implements IAuthService {
    private readonly jwtService;
    private readonly dao;
    private readonly userDao;
    private readonly otpService;
    private readonly userService;
    private readonly mailService;
    authParams: BaseParams;
    constructor(jwtService: JwtService, dao: BaseDAO<Users>, userDao: UserDao, otpService: OtpService, userService: UserService, mailService: MailService);
    authenticate(login: string, password: string, otp?: string): Promise<GetUserDto>;
    generateAccessToken(user: Users): Promise<string>;
    checkUser(login: string, req: Request): Promise<Users>;
    private verifyPassword;
    private checkOtp;
    private sendOtpVerification;
}
