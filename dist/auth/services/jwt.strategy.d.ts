import { JwtPayload } from "../interfaces/jwt-payload.interface";
import { Users } from "src/users/entities/Users.entity";
import { AuthService } from "./auth.service";
import { Request } from "express";
declare const JwtStrategy_base: new (...args: any[]) => any;
export declare class JwtStrategy extends JwtStrategy_base {
    private readonly authService;
    constructor(authService: AuthService);
    validate(req: Request, payload: JwtPayload): Promise<Users>;
}
export {};
