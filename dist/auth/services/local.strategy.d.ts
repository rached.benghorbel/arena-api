import { AuthService } from './auth.service';
import { GetUserDto } from 'src/users/dto/users/get.dto';
declare const LocalStrategy_base: new (...args: any[]) => any;
export declare class LocalStrategy extends LocalStrategy_base {
    private authService;
    constructor(authService: AuthService);
    validate(req: Request, username: string, password: string): Promise<GetUserDto>;
}
export {};
