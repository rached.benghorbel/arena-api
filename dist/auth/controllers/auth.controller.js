"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("../services/auth.service");
const localAuthentication_guard_1 = require("../services/localAuthentication.guard");
const Users_entity_1 = require("../../users/entities/Users.entity");
const arena_service_1 = require("../../arena/services/arena/arena.service");
const convert_user_mapper_1 = require("../../users/mappers/convert-user.mapper");
const role_enum_1 = require("../../users/entities/role.enum");
let AuthController = class AuthController {
    constructor(authService, arenaService) {
        this.authService = authService;
        this.arenaService = arenaService;
    }
    async logIn(request, response) {
        const user = request.user;
        const userToGet = convert_user_mapper_1.ConvertUserMapper.convertUserToGetUserDto(user);
        if (user.role == role_enum_1.Role.ARENA) {
            userToGet.hasArena = (await this.arenaService.findArenaByUser(user.pk) != null);
        }
        const jwtToken = await this.authService.generateAccessToken(user);
        response.setHeader('token', jwtToken);
        return response.send(userToGet);
    }
};
__decorate([
    common_1.HttpCode(200),
    common_1.UseGuards(localAuthentication_guard_1.LocalAuthenticationGuard),
    common_1.Post(),
    __param(0, common_1.Request()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logIn", null);
AuthController = __decorate([
    common_1.Controller('authentication'),
    __metadata("design:paramtypes", [auth_service_1.AuthService,
        arena_service_1.ArenaService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map