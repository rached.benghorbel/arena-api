import { AuthService } from '../services/auth.service';
import { Response } from 'express';
import { ArenaService } from 'src/arena/services/arena/arena.service';
export declare class AuthController {
    private readonly authService;
    private readonly arenaService;
    constructor(authService: AuthService, arenaService: ArenaService);
    logIn(request: any, response: Response): Promise<Response<any>>;
}
