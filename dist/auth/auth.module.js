"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const app_config_1 = require("../app.config");
const auth_controller_1 = require("./controllers/auth.controller");
const base_dao_1 = require("../communs/dao/base.dao");
const auth_service_1 = require("./services/auth.service");
const passport_1 = require("@nestjs/passport");
const local_strategy_1 = require("./services/local.strategy");
const jwt_strategy_1 = require("./services/jwt.strategy");
const user_dao_1 = require("../users/dao/user.dao");
const users_service_1 = require("../users/services/users.service");
const mail_service_1 = require("../communs/services/mail/mail.service");
const otp_service_1 = require("../communs/services/otp/otp.service");
const arena_service_1 = require("../arena/services/arena/arena.service");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    common_1.Module({
        imports: [
            passport_1.PassportModule.register({
                defaultStrategy: 'jwt',
                property: 'user',
                session: false,
            }),
            jwt_1.JwtModule.register({
                secret: app_config_1.AppConfig.jwtSecret,
                signOptions: {
                    expiresIn: app_config_1.AppConfig.jwtLifeTime,
                },
            }),
        ],
        controllers: [auth_controller_1.AuthController],
        providers: [
            base_dao_1.BaseDAO,
            auth_service_1.AuthService,
            local_strategy_1.LocalStrategy,
            jwt_strategy_1.JwtStrategy,
            user_dao_1.UserDao,
            users_service_1.UserService,
            mail_service_1.MailService,
            otp_service_1.OtpService,
            arena_service_1.ArenaService
        ]
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map