import { Request } from "express";
import { Users } from "src/users/entities/Users.entity";
export interface RequestWithUser extends Request {
    user: Users;
}
