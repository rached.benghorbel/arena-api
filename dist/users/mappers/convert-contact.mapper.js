"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertContactMapper = void 0;
const get_dto_1 = require("../dto/contact/get.dto");
class ConvertContactMapper {
    static convertContactToGetContactDto(contact) {
        const contactToGet = new get_dto_1.GetContactDto();
        contactToGet.email = contact.email;
        contactToGet.fax = contact.fax;
        contactToGet.phoneNumber = contact.phoneNumber;
        return contactToGet;
    }
    static convertListOfContactToListContactDto(contacts) {
        const contactsToGet = [];
        contacts.forEach(contact => {
            contactsToGet.push(this.convertContactToGetContactDto(contact));
        });
        return contactsToGet;
    }
}
exports.ConvertContactMapper = ConvertContactMapper;
//# sourceMappingURL=convert-contact.mapper.js.map