import { GetUserDto } from "../dto/users/get.dto";
import { Users } from "../entities/Users.entity";
export declare abstract class ConvertUserMapper {
    static convertUserToGetUserDto(user: Users): GetUserDto;
}
