"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertUserMapper = void 0;
const get_dto_1 = require("../dto/users/get.dto");
const convert_contact_mapper_1 = require("./convert-contact.mapper");
class ConvertUserMapper {
    static convertUserToGetUserDto(user) {
        const userToGet = new get_dto_1.GetUserDto();
        userToGet.contact = convert_contact_mapper_1.ConvertContactMapper.convertContactToGetContactDto(user === null || user === void 0 ? void 0 : user.contact);
        userToGet.firstName = user === null || user === void 0 ? void 0 : user.firstName;
        userToGet.lastName = user === null || user === void 0 ? void 0 : user.lastName;
        userToGet.role = user === null || user === void 0 ? void 0 : user.role;
        return userToGet;
    }
}
exports.ConvertUserMapper = ConvertUserMapper;
//# sourceMappingURL=convert-user.mapper.js.map