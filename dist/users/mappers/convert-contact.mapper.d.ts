import { GetContactDto } from "../dto/contact/get.dto";
import { Contact } from "../entities/contact.entity";
export declare abstract class ConvertContactMapper {
    static convertContactToGetContactDto(contact: Contact): GetContactDto;
    static convertListOfContactToListContactDto(contacts: Array<Contact>): Array<GetContactDto>;
}
