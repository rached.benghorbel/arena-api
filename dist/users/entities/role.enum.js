"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Role = void 0;
var Role;
(function (Role) {
    Role["USER"] = "USER";
    Role["ARENA"] = "ARENA";
    Role["SUPER_ADMIN"] = "SUPER_ADMIN";
})(Role = exports.Role || (exports.Role = {}));
//# sourceMappingURL=role.enum.js.map