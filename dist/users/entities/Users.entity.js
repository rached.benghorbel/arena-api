"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Users = void 0;
const typeorm_1 = require("typeorm");
const base_entity_1 = require("../../communs/entities/base-entity");
const create_dto_1 = require("../dto/users/create.dto");
const contact_entity_1 = require("./contact.entity");
const role_enum_1 = require("./role.enum");
const reservation_entity_1 = require("../../arena/entities/reservation.entity");
let Users = class Users extends base_entity_1.BaseEntity {
    constructor(user) {
        super();
        this.password = (user) ? user.password : null;
        this.firstName = (user) ? user.firstName : null;
        this.lastName = (user) ? user.lastName : null;
        this.contact = (user) ? new contact_entity_1.Contact(user.contact) : null;
        this.isArena = user === null || user === void 0 ? void 0 : user.isArena;
    }
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Users.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Users.prototype, "firstName", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Users.prototype, "lastName", void 0);
__decorate([
    typeorm_1.Column({ default: false, type: "tinyint" }),
    __metadata("design:type", Number)
], Users.prototype, "isActive", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], Users.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], Users.prototype, "isArena", void 0);
__decorate([
    typeorm_1.Column({ default: role_enum_1.Role.USER }),
    __metadata("design:type", String)
], Users.prototype, "role", void 0);
__decorate([
    typeorm_1.OneToOne(() => contact_entity_1.Contact, contact => contact.user, { cascade: true }),
    typeorm_1.JoinColumn(),
    __metadata("design:type", contact_entity_1.Contact)
], Users.prototype, "contact", void 0);
__decorate([
    typeorm_1.OneToMany(() => reservation_entity_1.Reservation, reservation => reservation.reservedBy),
    __metadata("design:type", Array)
], Users.prototype, "reservations", void 0);
Users = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [create_dto_1.CreateUserDto])
], Users);
exports.Users = Users;
//# sourceMappingURL=Users.entity.js.map