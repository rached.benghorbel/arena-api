"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Contact = void 0;
const arena_entity_1 = require("../../arena/entities/arena.entity");
const base_entity_1 = require("../../communs/entities/base-entity");
const typeorm_1 = require("typeorm");
const Users_entity_1 = require("./Users.entity");
let Contact = class Contact extends base_entity_1.BaseEntity {
    constructor(contact) {
        super();
        this.phoneNumber = (contact) ? contact.phoneNumber : null;
        this.email = (contact) ? contact.email : null;
        this.fax = (contact) ? contact.fax : null;
    }
};
__decorate([
    typeorm_1.Column({ unique: true, nullable: true }),
    __metadata("design:type", Number)
], Contact.prototype, "phoneNumber", void 0);
__decorate([
    typeorm_1.Column({ unique: true, nullable: true }),
    __metadata("design:type", String)
], Contact.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ unique: true, nullable: true }),
    __metadata("design:type", String)
], Contact.prototype, "fax", void 0);
__decorate([
    typeorm_1.OneToOne(() => Users_entity_1.Users, user => user.contact),
    __metadata("design:type", Users_entity_1.Users)
], Contact.prototype, "user", void 0);
__decorate([
    typeorm_1.ManyToOne(() => arena_entity_1.Arena, arena => arena.contacts),
    __metadata("design:type", arena_entity_1.Arena)
], Contact.prototype, "arena", void 0);
Contact = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], Contact);
exports.Contact = Contact;
//# sourceMappingURL=contact.entity.js.map