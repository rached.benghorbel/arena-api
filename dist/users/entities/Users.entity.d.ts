import { BaseEntity } from "src/communs/entities/base-entity";
import { CreateUserDto } from '../dto/users/create.dto';
import { Contact } from './contact.entity';
import { Role } from './role.enum';
import { Reservation } from 'src/arena/entities/reservation.entity';
export declare class Users extends BaseEntity {
    password: string;
    firstName: string;
    lastName: string;
    isActive: number;
    id?: string;
    isArena: boolean;
    role: Role;
    contact: Contact;
    reservations: Array<Reservation>;
    constructor();
    constructor(user: CreateUserDto);
}
