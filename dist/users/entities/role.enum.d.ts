export declare enum Role {
    USER = "USER",
    ARENA = "ARENA",
    SUPER_ADMIN = "SUPER_ADMIN"
}
