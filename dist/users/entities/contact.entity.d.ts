import { Arena } from "src/arena/entities/arena.entity";
import { BaseEntity } from "src/communs/entities/base-entity";
import { CreateContactDto } from "../dto/contact/create.dto";
import { GetContactDto } from "../dto/contact/get.dto";
import { Users } from "./Users.entity";
export declare class Contact extends BaseEntity {
    phoneNumber: number;
    email: string;
    fax: string;
    user: Users;
    arena: Arena;
    constructor();
    constructor(contact: CreateContactDto | GetContactDto);
}
