"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const Users_entity_1 = require("../entities/Users.entity");
const common_1 = require("@nestjs/common");
const base_dao_1 = require("../../communs/dao/base.dao");
const base_params_1 = require("../../communs/entities/base-params");
const bcrypt = require("bcrypt");
const app_config_1 = require("../../app.config");
const get_dto_1 = require("../dto/users/get.dto");
const mail_service_1 = require("../../communs/services/mail/mail.service");
const mail_entity_1 = require("../../communs/entities/mail-entity");
const helper_1 = require("../../communs/helpers/helper");
const user_dao_1 = require("../dao/user.dao");
const role_enum_1 = require("../entities/role.enum");
let UserService = class UserService {
    constructor(dao, mailService, userDao) {
        this.dao = dao;
        this.mailService = mailService;
        this.userDao = userDao;
        this.userBaseParams = new base_params_1.BaseParams();
        this.userBaseParams.currentRepository = 'Users';
        this.userBaseParams.relations = ['contact'];
        this.dao.setBaseParams(this.userBaseParams);
    }
    sendEmailToUser(user, generatedId) {
        const mailToSend = new mail_entity_1.Mail();
        mailToSend.mailFrom = 'noreply@arena.com';
        mailToSend.mailTo = user.contact.email;
        mailToSend.mailSubject = 'Welcome to arena',
            mailToSend.mailContext = {
                password: user.password,
                firstName: user.firstName,
                lastName: user.lastName,
                id: generatedId,
                appName: app_config_1.AppConfig.appName
            };
        this.mailService.sendMail(mailToSend);
    }
    async addUser(user) {
        if (!user.contact.email
            && !user.contact.phoneNumber) {
            throw new common_1.HttpException('Please provide an email or a phone number', common_1.HttpStatus.BAD_REQUEST);
        }
        const userToCreate = new Users_entity_1.Users(user);
        const cryptedPass = await bcrypt.hash(user.password, app_config_1.AppConfig.saltRounds);
        const currentUser = await this.userDao.findUserByEmail((user.contact.email) ? user.contact.email : user.contact.phoneNumber.toString());
        if (currentUser) {
            throw new common_1.HttpException("User already exists", common_1.HttpStatus.BAD_REQUEST);
        }
        if (cryptedPass) {
            userToCreate.password = cryptedPass;
            const userId = await bcrypt.hash(helper_1.Helper.makeId(185), app_config_1.AppConfig.saltRounds);
            userToCreate.id = userId;
            if (user.isArena) {
                userToCreate.role = role_enum_1.Role.ARENA;
            }
            const createdUser = new get_dto_1.GetUserDto(await this.dao.add(userToCreate));
            if (createdUser) {
                return createdUser;
            }
        }
    }
    async updateUser(user) {
        return new get_dto_1.GetUserDto(await this.dao.update(user, user.pk));
    }
    activateUser(user) {
        user.isActive = 1;
        this.updateUser(user);
    }
    async validateUser(id) {
        const currentUser = await this.dao.findOne({ id: id });
        if (!currentUser) {
            throw new common_1.HttpException('Invalid user id', common_1.HttpStatus.BAD_REQUEST);
        }
        currentUser.isActive = 1;
        currentUser.id = null;
        this.updateUser(currentUser);
        return { message: 'Account validated successfuly' };
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [base_dao_1.BaseDAO,
        mail_service_1.MailService,
        user_dao_1.UserDao])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=users.service.js.map