import { IUsersService } from "../interfaces/iuser-service.interface";
import { Users } from "../entities/Users.entity";
import { BaseDAO } from "src/communs/dao/base.dao";
import { CreateUserDto } from "../dto/users/create.dto";
import { GetUserDto } from "../dto/users/get.dto";
import { MailService } from "src/communs/services/mail/mail.service";
import { UserDao } from "../dao/user.dao";
export declare class UserService implements IUsersService {
    private readonly dao;
    private readonly mailService;
    private readonly userDao;
    private userBaseParams;
    constructor(dao: BaseDAO<Users>, mailService: MailService, userDao: UserDao);
    private sendEmailToUser;
    addUser(user: CreateUserDto): Promise<GetUserDto>;
    updateUser(user: Users): Promise<GetUserDto>;
    activateUser(user: Users): void;
    validateUser(id: string): Promise<any>;
}
