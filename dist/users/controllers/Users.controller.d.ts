import { Users } from "../entities/Users.entity";
import { UserService } from "../services/users.service";
import { CreateUserDto } from "../dto/users/create.dto";
import { GetUserDto } from "../dto/users/get.dto";
export declare class UsersController {
    private readonly userService;
    constructor(userService: UserService);
    addUser(user: CreateUserDto): Promise<GetUserDto>;
    updateUser(user: Users): Promise<GetUserDto>;
    validateUser(id: string): Promise<any>;
}
