import { Role } from "src/users/entities/role.enum";
import { Users } from "src/users/entities/Users.entity";
import { GetContactDto } from "../contact/get.dto";
export declare class GetUserDto {
    firstName: string;
    lastName: string;
    contact: GetContactDto;
    role: Role;
    hasArena?: boolean;
    constructor();
    constructor(user: Users);
}
