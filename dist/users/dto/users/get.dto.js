"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetUserDto = void 0;
const class_transformer_1 = require("class-transformer");
const role_enum_1 = require("../../entities/role.enum");
const Users_entity_1 = require("../../entities/Users.entity");
const get_dto_1 = require("../contact/get.dto");
class GetUserDto {
    constructor(user) {
        this.firstName = user === null || user === void 0 ? void 0 : user.firstName;
        this.lastName = user === null || user === void 0 ? void 0 : user.lastName;
        this.contact = new get_dto_1.GetContactDto(user === null || user === void 0 ? void 0 : user.contact);
        this.role = user === null || user === void 0 ? void 0 : user.role;
    }
}
__decorate([
    class_transformer_1.Type(() => get_dto_1.GetContactDto),
    __metadata("design:type", get_dto_1.GetContactDto)
], GetUserDto.prototype, "contact", void 0);
exports.GetUserDto = GetUserDto;
//# sourceMappingURL=get.dto.js.map