import { CreateContactDto } from "../contact/create.dto";
export declare class CreateUserDto {
    firstName: string;
    lastName: string;
    password: string;
    contact: CreateContactDto;
    isArena?: boolean;
}
