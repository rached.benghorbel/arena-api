export declare class CreateContactDto {
    email: string;
    phoneNumber: number;
    fax?: string;
}
