"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetContactDto = void 0;
const contact_entity_1 = require("../../entities/contact.entity");
class GetContactDto {
    constructor(contact) {
        this.email = contact === null || contact === void 0 ? void 0 : contact.email;
        this.fax = contact === null || contact === void 0 ? void 0 : contact.fax;
        this.phoneNumber = contact === null || contact === void 0 ? void 0 : contact.phoneNumber;
    }
}
exports.GetContactDto = GetContactDto;
//# sourceMappingURL=get.dto.js.map