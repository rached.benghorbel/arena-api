import { Contact } from "src/users/entities/contact.entity";
export declare class GetContactDto {
    email: string;
    phoneNumber?: number;
    fax?: string;
    constructor();
    constructor(contact: Contact);
}
