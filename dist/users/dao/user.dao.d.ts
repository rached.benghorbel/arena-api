import { Users } from "../entities/Users.entity";
import { IUserDao } from "./user.dao.interface";
export declare class UserDao implements IUserDao {
    findUserByEmail(email: string): Promise<Users>;
}
