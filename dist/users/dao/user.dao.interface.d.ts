import { Users } from "../entities/Users.entity";
export interface IUserDao {
    findUserByEmail(email: string): Promise<Users>;
}
