"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDao = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const Users_entity_1 = require("../entities/Users.entity");
let UserDao = class UserDao {
    async findUserByEmail(email) {
        if (!email)
            throw new common_1.HttpException('Please provide an email or a phone number', common_1.HttpStatus.BAD_REQUEST);
        try {
            const query = typeorm_1.getManager()
                .getRepository(Users_entity_1.Users)
                .createQueryBuilder('user')
                .leftJoinAndSelect('user.contact', 'contact');
            if (isNaN(Number.parseInt(email))) {
                query.where('contact.email = :givenMail', { givenMail: email });
            }
            else {
                query.where('contact.phoneNumber = :givenPhone', { givenPhone: Number.parseInt(email) });
            }
            return await query.getOne();
        }
        catch (exception) {
            throw new common_1.HttpException(exception, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
};
UserDao = __decorate([
    common_1.Injectable()
], UserDao);
exports.UserDao = UserDao;
//# sourceMappingURL=user.dao.js.map