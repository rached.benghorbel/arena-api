export declare const AppConfig: {
    globalPath: string;
    portToListen: string | number;
    saltRounds: number;
    jwtSecret: string;
    jwtLifeTime: string;
    appName: string;
    otpDigits: number;
    otpStep: number;
};
