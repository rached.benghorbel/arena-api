import { ConvertFileEntityMapper } from "src/communs/mappers/convert-file-entity.mapper";
import { CreateStadiumDto } from "../dto/stadium/create.dto";
import { GetStadiumDto } from "../dto/stadium/get.dto";
import { UpdateStadiumDto } from "../dto/stadium/update.dto";
import { StadiumFiles } from "../entities/stadium-files.entity";
import { Stadium } from "../entities/stadium.entity";
import { ConvertArenaMapper } from "./convert-arena.mapper";
import { StadiumFilesMapper } from "./stadium-files.mapper";

export class ConvertStadiumMapper{

    public static convertToStadium(stadium?: CreateStadiumDto): Stadium{
        if(!stadium) {
            return null;
        }
        const newStadium: Stadium = new Stadium();
        newStadium.name = stadium?.name;
        newStadium.description = stadium.description;
        newStadium.isAvailable = stadium.isAvailable;
        newStadium.playerNumber = stadium.playerNumber;
        newStadium.arena = ConvertArenaMapper.convertToArena(stadium.arena);
        newStadium.arena.pk = stadium.arena.code;
        newStadium.pictures = StadiumFilesMapper.convertFileEntityDtoToArenaFiles(stadium.pictures);
        return newStadium;
    }

    public static convertToGetStadiumDto(stadium?: Stadium): GetStadiumDto{
        if(!stadium) {
            return null;
        }
        const convertedStadium: GetStadiumDto = new GetStadiumDto();
        convertedStadium.code = stadium?.pk;
        convertedStadium.name = stadium?.name;
        convertedStadium.description = stadium.description;
        convertedStadium.playerNumber = stadium?.playerNumber;
        convertedStadium.isAvailable = stadium?.isAvailable;
        convertedStadium.pictures = ConvertFileEntityMapper.toDtoList(stadium?.pictures);
        return convertedStadium;
    }

    public static convertListOfStadiumsToGetStadiumDto (stadiums?: Array<Stadium>): Array<GetStadiumDto> {
        const getStadiumsDto: Array<GetStadiumDto> = new Array<GetStadiumDto>();
        stadiums.forEach(stadium => {
            getStadiumsDto.push(this.convertToGetStadiumDto(stadium));
        });
        return getStadiumsDto;
    }

    public static convertUpdateToStadium(stadium?: UpdateStadiumDto, stadiumToUpdate?: Stadium): Stadium {
        if(stadium.description) {
            stadiumToUpdate.description = stadium.description
        }

        if(stadium.name) {
            stadiumToUpdate.name = stadium.name;
        }

        if(stadium.isAvailable !== null) {
            stadiumToUpdate.isAvailable = stadium.isAvailable;
        }

        if(stadium.playerNumber) {
            stadiumToUpdate.playerNumber = stadium.playerNumber;
        }

        if(stadium.pictures) {
            stadiumToUpdate.pictures = StadiumFilesMapper.convertFileEntityDtoToArenaFiles(stadium.pictures);
        }

        return stadiumToUpdate;
    }

}