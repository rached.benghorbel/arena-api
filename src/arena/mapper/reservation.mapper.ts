import { ReservationCreateDto } from "../dto/reservation/create.dto";
import { GetReservationDto } from "../dto/reservation/get.dto";
import { Reservation } from "../entities/reservation.entity";

export abstract class ReservationMapper {
    
    public static toCreateDto(reservation: Reservation): ReservationCreateDto {
        const createReservation: ReservationCreateDto = new ReservationCreateDto();
        createReservation.reservationStartHour = reservation.reservationStartHour.toDateString();
        createReservation.reservationEndHour = reservation.reservationEndHour.toDateString();
        createReservation.reservedStadium = reservation.reservedStadium.pk;
        return createReservation;
    }

    public static createReservationDtoToReservation(createReservation: ReservationCreateDto): Reservation {
        const reservation: Reservation = new Reservation();
        reservation.reservationStartHour = new Date(createReservation.reservationStartHour);
        reservation.reservationEndHour = new Date(createReservation.reservationEndHour);
        return reservation;
    }

    public static toGetDto(reservation: Reservation): GetReservationDto {
        const reservationToGet: GetReservationDto = new GetReservationDto();
        reservationToGet.reservationEndHour = reservation.reservationEndHour;
        reservationToGet.reservationStartHour = reservation.reservationStartHour;
        return reservationToGet;
    }
}