import { ConvertFileEntityMapper } from 'src/communs/mappers/convert-file-entity.mapper';
import { GetContactDto } from 'src/users/dto/contact/get.dto';
import { Contact } from 'src/users/entities/contact.entity';
import { CreateArenaDto } from '../dto/arena/create.dto';
import { GetArenaDto } from '../dto/arena/get.dto';
import { UpdateArenaDto } from '../dto/arena/update.dto';
import { Arena } from '../entities/arena.entity';
import { ArenaFilesMapper } from './arena-files.mapper';

export class ConvertArenaMapper {
  public static convertToArena(arena?: CreateArenaDto | GetArenaDto): Arena {
    const arenaConverted: Arena = new Arena();
    arenaConverted.name = arena?.name;
    arenaConverted.adress = arena?.adress;
    arenaConverted.district = arena?.district;
    arenaConverted.endHour = arena?.endHour;
    arenaConverted.longitude = arena?.longitude;
    arenaConverted.latitude = arena?.latitude;
    arenaConverted.isOpen = arena?.isOpen;
    arenaConverted.maxHours = arena?.maxHours;
    arenaConverted.startHour = arena?.startHour;
    arenaConverted.contacts = [];
    arenaConverted.pictures = ArenaFilesMapper.convertFileEntityDtoToArenaFiles(arena?.pictures);
    arena.contacts?.forEach(contact =>
      arenaConverted.contacts.push(new Contact(contact)),
    );
    return arenaConverted;
  }

  public static convertToGetArenaDto(arena?: Arena): GetArenaDto {
    if (!arena) return null;
    const arenaToGet: GetArenaDto = new GetArenaDto();
    arenaToGet.contacts = [];
    arenaToGet.code = arena?.pk;
    arenaToGet.name = arena?.name;
    arenaToGet.adress = arena?.adress;
    arenaToGet.district = arena?.district;
    arenaToGet.endHour = arena?.endHour;
    arenaToGet.longitude = arena?.longitude;
    arenaToGet.latitude = arena?.latitude;
    arenaToGet.isOpen = arena?.isOpen;
    arenaToGet.maxHours = arena?.maxHours;
    arenaToGet.startHour = arena?.startHour;
    arenaToGet.pictures = ConvertFileEntityMapper.toDtoList(arena.pictures);
    arena?.contacts?.forEach(contact =>
      arenaToGet.contacts.push(new GetContactDto(contact)),
    );
    return arenaToGet;
  }

  public static convertUpdateArenaDtoToArena(
    arena?: UpdateArenaDto,
    arenaToUpdate?: Arena,
  ): Arena {
    if (!arena) return null;
    if (arena.adress) {
      arenaToUpdate.adress = arena.adress;
    }
    if (arena.longitude) {
      arenaToUpdate.longitude = arena.longitude;
    }
    if (arena.latitude) {
      arenaToUpdate.latitude = arena.latitude;
    }
    if (arena.name) {
      arenaToUpdate.name = arena.name;
    }
    if (arena.startHour) {
      arenaToUpdate.startHour = arena.startHour;
    }
    if (arena.maxHours) {
      arenaToUpdate.maxHours = arena.maxHours;
    }
    if (arena.isOpen) {
      arenaToUpdate.isOpen = arena.isOpen;
    }
    if (arena.district) {
      arenaToUpdate.district = arena.district;
    }
    if (arena.isSubscribed) {
      arenaToUpdate.isSubscribed = arena.isSubscribed;
    }
    if (arena.endHour) {
      arenaToUpdate.endHour = arena.endHour;
    }
    if(arena.pictures) {
      arenaToUpdate.pictures = arena.pictures;
    }
    return arenaToUpdate;
  }
}
