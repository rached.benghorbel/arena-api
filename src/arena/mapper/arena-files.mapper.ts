import { FileEntityDto } from "src/communs/dto/file-entity.dto";
import { ArenaFiles } from "../entities/arena-files.entity";

export abstract class ArenaFilesMapper {
    public static toArenaFiles(file: FileEntityDto): ArenaFiles {
        const arenaFile: ArenaFiles = new ArenaFiles();
        if(file.pk) arenaFile.pk = file.pk;
        arenaFile.content = file.content;
        arenaFile.extension = file.extension;
        arenaFile.name = file.name;
        arenaFile.path = file.path;
        arenaFile.size = file.size;
        return arenaFile;
    }
    public static convertFileEntityDtoToArenaFiles( files: Array<FileEntityDto> ): Array<ArenaFiles> {
        const arenaFiles: Array<ArenaFiles> = new Array<ArenaFiles>();
        files?.forEach((file) => {
            arenaFiles.push(this.toArenaFiles(file));
        })
        return arenaFiles;
    }
}