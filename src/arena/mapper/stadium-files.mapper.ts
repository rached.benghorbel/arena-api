import { FileEntityDto } from "src/communs/dto/file-entity.dto";
import { StadiumFiles } from "../entities/stadium-files.entity";

export abstract class StadiumFilesMapper {
    public static toArenaFiles(file: FileEntityDto): StadiumFiles {
        const stadiumFile: StadiumFiles = new StadiumFiles();
        if(file.pk) stadiumFile.pk = file.pk;
        stadiumFile.content = file.content;
        stadiumFile.extension = file.extension;
        stadiumFile.name = file.name;
        stadiumFile.path = file.path;
        stadiumFile.size = file.size;
        return stadiumFile;
    }
    public static convertFileEntityDtoToArenaFiles( files: Array<FileEntityDto> ): Array<StadiumFiles> {
        const stadiumFiles: Array<StadiumFiles> = new Array<StadiumFiles>();
        files.forEach((file) => {
            stadiumFiles.push(this.toArenaFiles(file));
        })
        return stadiumFiles;
    }
}