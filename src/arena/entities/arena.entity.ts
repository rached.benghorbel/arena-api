import { BaseEntity } from "src/communs/entities/base-entity";
import { CreateContactDto } from "src/users/dto/contact/create.dto";
import { Contact } from "src/users/entities/contact.entity";
import { Column, Entity, JoinTable, OneToMany } from "typeorm";
import { CreateArenaDto } from "../dto/arena/create.dto";
import { ArenaFiles } from "./arena-files.entity";
import { MaxHours } from "./max-hours.enum";
import { Stadium } from "./stadium.entity";

@Entity()
export class Arena extends BaseEntity{
    @Column()
    name: string;    
    @Column({ type: "float", nullable: true })
    longitude?: number;
    @Column({ type: "float", nullable: true })
    latitude?: number;
    @Column({ type: "timestamp" })
    startHour: Date;
    @Column({ type: "timestamp" })
    endHour: Date;
    @Column()
    adress: string;
    @Column({ nullable: true })
    district?: string;
    @Column({ default: true })
    isOpen?: boolean;
    @Column({ default: true })
    isSubscribed?: boolean;
    @Column({ default: MaxHours.ONE_HOUR })
    maxHours?: MaxHours;
    @OneToMany(() => Contact, contact => contact.arena, 
    {
        cascade: true,
        onUpdate: 'CASCADE', 
        onDelete: 'CASCADE'
    })
    contacts: Array<Contact>

    @OneToMany(() => Stadium, stadium => stadium.arena)
    stadiums: Array<Stadium>;

    @OneToMany(
        type => ArenaFiles, 
        photo => photo.arena, 
        {
            eager: true,
            cascade: true,
            onUpdate: 'CASCADE', 
            onDelete: 'CASCADE'
        }
    )
    @JoinTable()
    public pictures: ArenaFiles[];

    constructor();
    constructor(arena: CreateArenaDto);
    constructor(arena?: CreateArenaDto){
        super();
        this.name = arena?.name;
        this.adress = arena?.adress;
        this.district = arena?.district;
        this.endHour = arena?.endHour;
        this.longitude = arena?.longitude;
        this.latitude = arena?.latitude;
        this.isOpen = arena?.isOpen;
        this.maxHours = arena?.maxHours;
        this.startHour = arena?.startHour;
        this.contacts = this.convertToContactsObject(arena?.contacts);
    }

    private convertToContactsObject(contacts?: Array<CreateContactDto>): Array<Contact>{
        if(!contacts) return null;
        let convertedContacts: Array<Contact>;
        contacts.forEach(contact => convertedContacts.push(new Contact(contact)));
        return convertedContacts;
    }

}