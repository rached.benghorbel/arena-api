import { FileEntity } from "src/communs/entities/file-entity";
import { Entity, ManyToOne } from "typeorm";
import { Stadium } from "./stadium.entity";

@Entity()
export class StadiumFiles extends FileEntity {
    @ManyToOne(type => Stadium, stadium => stadium.pictures)
    stadium: Stadium;
}