import { BaseEntity } from "src/communs/entities/base-entity";
import { Users } from "src/users/entities/Users.entity";
import { Column, Entity, JoinTable, ManyToOne, OneToMany } from "typeorm";
import { Stadium } from "./stadium.entity";

@Entity()
export class Reservation extends BaseEntity {
    @Column({ type: 'timestamp' })
    reservationStartHour: Date;
    @Column({ type: 'timestamp' })
    reservationEndHour: Date;
    @ManyToOne(() => Stadium, stadium => stadium.reservations)
    reservedStadium: Stadium;
    @OneToMany(
        () => Users,
        user => user.reservations
    )
    reservedBy: Users;
}