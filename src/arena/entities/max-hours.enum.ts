export enum MaxHours{
    ONE_HOUR = 'ONE_HOUR',
    ONE_AND_HAL_HOUR= 'ONE_AND_HALF_HOUR',
    TWO_HOURS = 'TWO_HOURS'
}