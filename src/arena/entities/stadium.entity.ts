import { BaseEntity } from "src/communs/entities/base-entity";
import { Column, Entity, JoinTable, ManyToOne, OneToMany } from "typeorm";
import { Arena } from "./arena.entity";
import { Reservation } from "./reservation.entity";
import { StadiumFiles } from "./stadium-files.entity";

@Entity()
export class Stadium extends BaseEntity{
    @Column({ nullable: true })
    name?: string;
    @Column({ nullable: true })
    description?: string;
    @Column()
    playerNumber: number;
    @Column()
    isAvailable?: boolean = true;
    @ManyToOne(() => Arena, arena => arena.stadiums)
    arena: Arena;
    @OneToMany(
        () => Reservation,
        reservation => reservation.reservedStadium
    )
    reservations: Array<Reservation>;
    @OneToMany(
        () => StadiumFiles, 
        photo => photo.stadium, 
        {
            eager: true,
            cascade: true,
            onUpdate: 'CASCADE', 
            onDelete: 'CASCADE'
        }
    )
    @JoinTable()
    public pictures: Array<StadiumFiles>; 
}