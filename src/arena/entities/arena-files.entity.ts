import { FileEntity } from "src/communs/entities/file-entity";
import { Entity, ManyToOne } from "typeorm";
import { Arena } from "./arena.entity";

@Entity()
export class ArenaFiles extends FileEntity {
    @ManyToOne(type => Arena, arena => arena.pictures)
    public arena : Arena;
}