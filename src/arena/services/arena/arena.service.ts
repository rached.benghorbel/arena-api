import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateArenaDto } from 'src/arena/dto/arena/create.dto';
import { GetArenaDto } from 'src/arena/dto/arena/get.dto';
import { UpdateArenaDto } from 'src/arena/dto/arena/update.dto';
import { Arena } from 'src/arena/entities/arena.entity';
import { ConvertArenaMapper } from 'src/arena/mapper/convert-arena.mapper';
import { BaseDAO } from 'src/communs/dao/base.dao';
import { BaseParams } from 'src/communs/entities/base-params';
import { Helper } from 'src/communs/helpers/helper';
import { IArenaService } from './arena.service.interface';

@Injectable()
export class ArenaService implements IArenaService {

    private arenaBaseParms: BaseParams = new BaseParams();

    constructor(private readonly dao: BaseDAO<Arena>){
        this.arenaBaseParms.currentRepository = 'Arena';
        this.arenaBaseParms.relations = [ 'contacts' ];
        this.dao.setBaseParams(this.arenaBaseParms); 
    }
    findArenaByUser(userId: string): Promise<Arena> {
        return this.dao.findOne({ 'createdU': userId });
    }

    async createArena(arena: CreateArenaDto): Promise<GetArenaDto> {
        if(await this.findArenaByUser(Helper.currentUser.pk)) {
            throw new HttpException('User already have arena', HttpStatus.FORBIDDEN);
        }
        return ConvertArenaMapper.convertToGetArenaDto(
            await this.dao.add(ConvertArenaMapper.convertToArena(arena))
        );
    }

    async getArenaDetail(code: string): Promise<GetArenaDto> {
        const arena: GetArenaDto =  ConvertArenaMapper.convertToGetArenaDto(
            await this.dao.findOne({ pk: code },true)
        );
        if(!arena){
            throw new HttpException('The requested arena was not found', HttpStatus.NOT_FOUND);
        }
        return arena;
    }

    async listArena(): Promise<GetArenaDto[]> {
        const arenaList: Array<Arena> = await this.dao.findAll(true);
        const arenaListToGet : Array<GetArenaDto> = [];
        arenaList.forEach( arena => arenaListToGet.push(ConvertArenaMapper.convertToGetArenaDto(arena)) );
        return arenaListToGet;
    }

    async updateArena(arena: UpdateArenaDto): Promise<GetArenaDto>{
        let arenaToUpdate: Arena = await this.dao.findOne({ pk: arena.code }, true);
        if(arenaToUpdate) {
            arenaToUpdate = ConvertArenaMapper.convertUpdateArenaDtoToArena(arena, arenaToUpdate);
        }
        else {
            throw new HttpException('The requested arena was not found', HttpStatus.NOT_FOUND);
        }
        return ConvertArenaMapper.convertToGetArenaDto(await this.dao.update(arenaToUpdate, arena.code));
    }

}
