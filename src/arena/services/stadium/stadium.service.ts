import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateStadiumDto } from 'src/arena/dto/stadium/create.dto';
import { GetStadiumDto } from 'src/arena/dto/stadium/get.dto';
import { UpdateStadiumDto } from 'src/arena/dto/stadium/update.dto';
import { Arena } from 'src/arena/entities/arena.entity';
import { Stadium } from 'src/arena/entities/stadium.entity';
import { ConvertStadiumMapper } from 'src/arena/mapper/convert-stadium.mapper';
import { BaseParams } from 'src/communs/entities/base-params';
import { StadiumDao } from '../../dao/stadium.dao';
import { IStadiumService } from './stadium.service.interface';

@Injectable()
export class StadiumService implements IStadiumService {

    private stadiumBaseParams: BaseParams = new BaseParams();
    constructor(
        private readonly dao: StadiumDao
    ){
        this.stadiumBaseParams.currentRepository = 'Stadium';
        this.stadiumBaseParams.relations= [ "arena" ];
        this.dao.setBaseParams(this.stadiumBaseParams);
    }

    async createStadium(stadium: CreateStadiumDto): Promise<GetStadiumDto> {
        return ConvertStadiumMapper.convertToGetStadiumDto(
            await this.dao.add(ConvertStadiumMapper.convertToStadium(stadium))
        );
    }
    
    async getAllStadiums(): Promise<GetStadiumDto[]> {
        const stadiumList: Array<GetStadiumDto> = [];
        const foundStadiums: Array<Stadium> = await this.dao.findAll();
        foundStadiums.forEach( stadium => stadiumList.push(
                ConvertStadiumMapper.convertToGetStadiumDto(stadium)
            )
        );
        return stadiumList;
    }
    
    async getStadium(stadiumId: string): Promise<GetStadiumDto> {
        const stadium: Stadium = await this.dao.findOne({ pk: stadiumId });
        if(!stadium){
            throw new HttpException('the requested stadium is not found', HttpStatus.NOT_FOUND);
        }
        return ConvertStadiumMapper.convertToGetStadiumDto(stadium);
    }

    async getStadiumByArena(arenaId: string): Promise<Array<GetStadiumDto>> {
        const arenaToFind: Arena = new Arena();
        arenaToFind.pk = arenaId;
        const stadiums: Array<Stadium> = await this.dao.findByParams({arena: arenaToFind});
        if(!stadiums) {
            throw new HttpException('the requested arena has no stadiums', HttpStatus.NOT_FOUND);
        }
        return ConvertStadiumMapper.convertListOfStadiumsToGetStadiumDto(stadiums);
    }
    
    async updateStadium(stadium: UpdateStadiumDto): Promise<GetStadiumDto> {
        let stadiumToUpdate: Stadium = await this.dao.findOne({ pk: stadium.code });
        if(!stadiumToUpdate) {
            throw new HttpException('the requested stadium is not found', HttpStatus.NOT_FOUND);
        }
        stadiumToUpdate = await this.dao.update(ConvertStadiumMapper.convertUpdateToStadium(stadium,stadiumToUpdate), stadiumToUpdate.pk);        
        return ConvertStadiumMapper.convertToGetStadiumDto(stadiumToUpdate);
    }
}
