import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ReservationDao } from 'src/arena/dao/reservation.dao';
import { StadiumDao } from 'src/arena/dao/stadium.dao';
import { ReservationCreateDto } from 'src/arena/dto/reservation/create.dto';
import { GetReservationDto } from 'src/arena/dto/reservation/get.dto';
import { Reservation } from 'src/arena/entities/reservation.entity';
import { Stadium } from 'src/arena/entities/stadium.entity';
import { ReservationMapper } from 'src/arena/mapper/reservation.mapper';
import { BaseParams } from 'src/communs/entities/base-params';
import { Helper } from 'src/communs/helpers/helper';
import { IReservationService } from './reservation.interface';

@Injectable()
export class ReservationService implements IReservationService {
    
    private reservationBaseParams: BaseParams = new BaseParams();
    private stadiumBaseParams: BaseParams = new BaseParams();

    constructor(
        private readonly dao: ReservationDao,
        private readonly stadiumDao: StadiumDao
    ) {
        this.reservationBaseParams.currentRepository = 'Reservation';
        this.reservationBaseParams.relations= [ 'stadium', 'users' ];
        this.dao.setBaseParams(this.reservationBaseParams);
        this.stadiumBaseParams.currentRepository = 'Stadium';
        this.stadiumDao.setBaseParams(this.stadiumBaseParams);
    }

    async create(reservationToCreate: ReservationCreateDto): Promise<GetReservationDto> {
        const reservation: Reservation = ReservationMapper.createReservationDtoToReservation(reservationToCreate);
        if(reservation.reservationStartHour.getTime() < Date.now()) {
            throw new HttpException(`the start hour must be > to ${Date.now()}`, HttpStatus.PRECONDITION_FAILED);
        }
        const stadium: Stadium = await this.stadiumDao.findOne({ pk: reservationToCreate.reservedStadium });
        if(!stadium) {
            throw new HttpException(`The stadium with id ${reservationToCreate.reservedStadium} was not found`, HttpStatus.NOT_FOUND);
        }
        reservation.reservedStadium = stadium;
        reservation.reservedBy = Helper.currentUser;
        return( ReservationMapper.toGetDto(await this.dao.add(reservation)));
    }

    get(reservationId: string) {
        throw new Error('Method not implemented.');
    }

    update(reservation: any) {
        throw new Error('Method not implemented.');
    }
}
