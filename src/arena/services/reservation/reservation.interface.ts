import { ReservationCreateDto } from "src/arena/dto/reservation/create.dto";
import { GetReservationDto } from "src/arena/dto/reservation/get.dto";

export interface IReservationService {
    
    create(reservation: ReservationCreateDto): Promise<GetReservationDto>;
    
    get(reservationId: string);
    
    update(reservation);
}