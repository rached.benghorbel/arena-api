import { Body, Controller, Get, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { CreateArenaDto } from 'src/arena/dto/arena/create.dto';
import { GetArenaDto } from 'src/arena/dto/arena/get.dto';
import { UpdateArenaDto } from 'src/arena/dto/arena/update.dto';
import { GetStadiumDto } from 'src/arena/dto/stadium/get.dto';
import { ArenaService } from 'src/arena/services/arena/arena.service';
import { StadiumService } from 'src/arena/services/stadium/stadium.service';
import { JwtAuthGuard } from 'src/auth/services/jwt-auth.guard';

@Controller('arena')
export class ArenaController {

    constructor(
        private readonly arenaService: ArenaService,
        private readonly stadiumService: StadiumService
    ){};

    @UseGuards(JwtAuthGuard)
    @Post()
    async createArena(@Body() arena: CreateArenaDto): Promise<GetArenaDto>{
        return await this.arenaService.createArena(arena);
    }
    
    @Get()
    async getArenaList(): Promise<Array<GetArenaDto>>{
        return await this.arenaService.listArena();
    }

    @Get('/:arenaCode')
    async getArena(@Param('arenaCode') code: string): Promise<GetArenaDto>{
        return await this.arenaService.getArenaDetail(code);
    }

    @Get('/stadiums/:arenaId')
    async getStadiumsByArena(@Param('arenaId') arenaId: string): Promise<Array<GetStadiumDto>>{
        return this.stadiumService.getStadiumByArena(arenaId);
    }

    @UseGuards(JwtAuthGuard)
    @Put()
    async updateArena(@Body() arena: UpdateArenaDto): Promise<GetArenaDto>{
        return await this.arenaService.updateArena(arena);
    }

}
