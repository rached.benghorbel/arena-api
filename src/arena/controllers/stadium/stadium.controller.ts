import { Body, Controller, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { CreateStadiumDto } from 'src/arena/dto/stadium/create.dto';
import { GetStadiumDto } from 'src/arena/dto/stadium/get.dto';
import { UpdateStadiumDto } from 'src/arena/dto/stadium/update.dto';
import { StadiumService } from 'src/arena/services/stadium/stadium.service';
import { JwtAuthGuard } from 'src/auth/services/jwt-auth.guard';
@UseGuards(JwtAuthGuard)
@Controller('stadium')
export class StadiumController {

    constructor(private readonly stadiumService: StadiumService){}

    @Post()
    async createStadium(@Body() stadium: CreateStadiumDto): Promise<GetStadiumDto> {
        return this.stadiumService.createStadium(stadium);
    }

    @Get()
    async getAllStadiums(): Promise<Array<GetStadiumDto>> {
        return this.stadiumService.getAllStadiums();
    }

    @Get('/:stadiumId')
    async getStadium(@Param('stadiumId') stadiumId: string): Promise<GetStadiumDto>{
        return this.stadiumService.getStadium(stadiumId);
    }

    @Put()
    async updateStadium(@Body() stadium: UpdateStadiumDto): Promise<GetStadiumDto> {
        return this.stadiumService.updateStadium(stadium);
    }

}
