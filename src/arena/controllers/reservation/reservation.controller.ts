import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ReservationCreateDto } from 'src/arena/dto/reservation/create.dto';
import { GetReservationDto } from 'src/arena/dto/reservation/get.dto';
import { ReservationService } from 'src/arena/services/reservation/reservation.service';
import { JwtAuthGuard } from 'src/auth/services/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('reservation')
export class ReservationController {

    constructor(private reservationService: ReservationService){}

    @Post()
    async reserve(@Body() reservation: ReservationCreateDto): Promise<GetReservationDto> {
       return this.reservationService.create(reservation);
    }
}
