import { Injectable } from "@nestjs/common";
import { Stadium } from "src/arena/entities/stadium.entity";
import { BaseDAO } from "src/communs/dao/base.dao";

@Injectable()
export class StadiumDao extends BaseDAO<Stadium> {}