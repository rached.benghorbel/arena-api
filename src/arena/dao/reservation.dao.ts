import { Injectable } from "@nestjs/common";
import { BaseDAO } from "src/communs/dao/base.dao";
import { Reservation } from "../entities/reservation.entity";

@Injectable()
export class ReservationDao extends BaseDAO<Reservation> {}