import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { BaseDAO } from 'src/communs/dao/base.dao';
import { ArenaController } from './controllers/arena/arena.controller';
import { StadiumController } from './controllers/stadium/stadium.controller';
import { ArenaService } from './services/arena/arena.service';
import { StadiumDao } from './dao/stadium.dao';
import { StadiumService } from './services/stadium/stadium.service';
import { ReservationController } from './controllers/reservation/reservation.controller';
import { ReservationService } from './services/reservation/reservation.service';
import { ReservationDao } from './dao/reservation.dao';

@Module({
    imports: [
        AuthModule
    ],
    controllers: [
        ReservationController,
        StadiumController,
        ArenaController
    ],
    providers: [
        StadiumService,
        ArenaService,
        ReservationService,
        BaseDAO,
        StadiumDao,
        ReservationDao
    ]
})
export class ArenaModule {}
