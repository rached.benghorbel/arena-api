import { IsNotEmpty } from "class-validator";
import { GetStadiumDto } from "../stadium/get.dto";

export class ReservationCreateDto {
    @IsNotEmpty()
    reservationStartHour: string;
    @IsNotEmpty()
    reservationEndHour: string;
    @IsNotEmpty()
    reservedStadium: string;
}