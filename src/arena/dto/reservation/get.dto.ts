export class GetReservationDto {
    reservationStartHour: Date;
    reservationEndHour: Date;
}