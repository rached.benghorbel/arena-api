import { IsArray, IsNotEmpty, IsOptional, ValidateNested } from "class-validator";
import { MaxHours } from "src/arena/entities/max-hours.enum";
import { CreateContactDto } from "src/users/dto/contact/create.dto";
import { Type } from "class-transformer";
import { ArenaFiles } from "src/arena/entities/arena-files.entity";

export class CreateArenaDto {
    @IsNotEmpty()
    name: string;
    @IsOptional()
    longitude?: number;
    @IsOptional()
    latitude?: number;
    @IsNotEmpty()
    startHour: Date;
    @IsNotEmpty()
    endHour: Date;
    @IsNotEmpty()
    adress: string;
    @IsOptional()
    district?: string;
    @IsOptional()
    isOpen?: boolean;
    @IsOptional()
    isSubscribed: boolean;
    @IsOptional()
    maxHours: MaxHours;
    
    @ValidateNested({ each: true })
    @Type(() => CreateContactDto)
    @IsArray()
    @IsNotEmpty()
    contacts: Array<CreateContactDto>;
    
    @IsArray()
    pictures: Array<ArenaFiles>;
}