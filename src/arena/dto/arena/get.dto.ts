import { Arena } from "src/arena/entities/arena.entity";
import { MaxHours } from "src/arena/entities/max-hours.enum";
import { FileEntityDto } from "src/communs/dto/file-entity.dto";
import { GetContactDto } from "src/users/dto/contact/get.dto";

export class GetArenaDto{
    code: string;
    name: string;    
    longitude?: number;
    latitude?: number;
    startHour: Date;
    endHour: Date;
    adress: string;
    district?: string;
    isOpen?: boolean;
    isSubscribed?: boolean;
    maxHours?: MaxHours;
    contacts: Array<GetContactDto>;
    pictures?: Array<FileEntityDto>;

    constructor();
    constructor(arena: Arena);
    constructor(arena?: Arena){
        this.code = arena?.pk;
        this.name = arena?.name;
        this.adress = arena?.adress;
        this.contacts = arena?.contacts;
        this.district = arena?.district;
        this.endHour = arena?.endHour;
        this.longitude = arena?.longitude;
        this.latitude = arena?.latitude;
        this.isOpen = arena?.isOpen;
        this.maxHours = arena?.maxHours;
        this.startHour = arena?.startHour;
    }

}