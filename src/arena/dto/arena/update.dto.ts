import { Type } from "class-transformer";
import { IsArray, IsNotEmpty, IsOptional, ValidateNested } from "class-validator";
import { ArenaFiles } from "src/arena/entities/arena-files.entity";
import { MaxHours } from "src/arena/entities/max-hours.enum";
import { CreateContactDto } from "src/users/dto/contact/create.dto";

export class UpdateArenaDto{
    @IsNotEmpty()
    code: string;
    @IsOptional()
    name?: string;
    @IsOptional()
    longitude?: number;
    @IsOptional()
    latitude?: number;
    @IsOptional()
    startHour: Date;
    @IsOptional()
    endHour?: Date;
    @IsOptional()
    adress?: string;
    @IsOptional()
    district?: string;
    @IsOptional()
    isOpen?: boolean;
    @IsOptional()
    isSubscribed: boolean;
    @IsOptional()
    maxHours: MaxHours;
    @ValidateNested({ each: true })
    @Type(() => CreateContactDto)
    @IsArray()
    @IsOptional()
    contacts: Array<CreateContactDto>;
    @IsArray()
    pictures: Array<ArenaFiles>;
}