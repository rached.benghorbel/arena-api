import { IsArray, IsNotEmpty, IsOptional } from "class-validator";
import { FileEntityDto } from "src/communs/dto/file-entity.dto";

export class UpdateStadiumDto {
    @IsNotEmpty()
    code: string;
    @IsOptional()
    name?: string;
    @IsOptional()
    description: string;
    @IsNotEmpty()
    playerNumber: number;
    @IsOptional()
    isAvailable: boolean;
    @IsArray()
    pictures: Array<FileEntityDto>;
}