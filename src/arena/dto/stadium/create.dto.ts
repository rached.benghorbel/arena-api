import { IsArray, IsNotEmpty, IsOptional } from "class-validator";
import { FileEntityDto } from "src/communs/dto/file-entity.dto";
import { GetArenaDto } from "../arena/get.dto";

export class CreateStadiumDto{
    @IsOptional()
    name?: string;
    @IsOptional()
    description?: string;
    @IsNotEmpty()
    playerNumber: number;
    @IsOptional()
    isAvailable: boolean;
    @IsNotEmpty()
    arena: GetArenaDto;
    @IsArray()
    pictures: Array<FileEntityDto>;
}