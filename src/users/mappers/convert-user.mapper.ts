import { GetUserDto } from "../dto/users/get.dto";
import { Users } from "../entities/Users.entity";
import { ConvertContactMapper } from "./convert-contact.mapper";

export abstract class ConvertUserMapper {
    
    public static convertUserToGetUserDto(user: Users): GetUserDto {
        const userToGet: GetUserDto = new GetUserDto();
        userToGet.contact = ConvertContactMapper.convertContactToGetContactDto(user?.contact);
        userToGet.firstName = user?.firstName;
        userToGet.lastName = user?.lastName;
        userToGet.role = user?.role;
        return userToGet;
    }

}