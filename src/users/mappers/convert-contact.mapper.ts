import { GetContactDto } from "../dto/contact/get.dto";
import { Contact } from "../entities/contact.entity";

export abstract class ConvertContactMapper {
    
    public static convertContactToGetContactDto(contact: Contact): GetContactDto {
        const contactToGet: GetContactDto = new GetContactDto();
        contactToGet.email = contact.email;
        contactToGet.fax = contact.fax;
        contactToGet.phoneNumber = contact.phoneNumber;
        return contactToGet;
    }

    public static convertListOfContactToListContactDto(contacts: Array<Contact>): Array<GetContactDto> {
        const contactsToGet: Array<GetContactDto> = [];
        contacts.forEach( contact => {
            contactsToGet.push(this.convertContactToGetContactDto(contact));
        });
        return contactsToGet;
    }

}