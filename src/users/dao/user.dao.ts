import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { getManager, SelectQueryBuilder } from "typeorm";
import { Users } from "../entities/Users.entity";
import { IUserDao } from "./user.dao.interface";

@Injectable()
export class UserDao implements IUserDao{
    
    async findUserByEmail(email: string): Promise<Users> {
        if(!email) 
            throw new HttpException('Please provide an email or a phone number', HttpStatus.BAD_REQUEST);
        try{
            const query: SelectQueryBuilder<Users> = getManager()
            .getRepository(Users)
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.contact', 'contact');
            if(isNaN(Number.parseInt(email))) {
                query.where('contact.email = :givenMail', { givenMail: email });
            } else {
                query.where('contact.phoneNumber = :givenPhone', { givenPhone: Number.parseInt(email) });
            }
            return await query.getOne();
        }catch(exception){
            throw new HttpException(exception, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }

}