import { Module } from '@nestjs/common';
import { UsersController } from './controllers/Users.controller';
import { BaseDAO } from 'src/communs/dao/base.dao';
import { UserService } from './services/users.service';
import { AuthModule } from 'src/auth/auth.module';
import { MailService } from 'src/communs/services/mail/mail.service';
import { UserDao } from './dao/user.dao';

@Module({
    imports:[ AuthModule ],
    controllers: [ UsersController ],
    providers:[
        BaseDAO,
        MailService,
        UserService,
        UserDao
    ]
})
export class UsersModule {}
