import { IUsersService } from "../interfaces/iuser-service.interface";
import { Users } from "../entities/Users.entity";
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { BaseDAO } from "src/communs/dao/base.dao";
import { BaseParams } from "src/communs/entities/base-params";
import * as bcrypt from 'bcrypt';
import { AppConfig } from "src/app.config";
import { CreateUserDto } from "../dto/users/create.dto";
import { GetUserDto } from "../dto/users/get.dto";
import { MailService } from "src/communs/services/mail/mail.service";
import { Mail } from "src/communs/entities/mail-entity";
import { Helper } from "src/communs/helpers/helper";
import { UserDao } from "../dao/user.dao";
import { Role } from "../entities/role.enum";

@Injectable()
export class UserService implements IUsersService{
    private userBaseParams: BaseParams = new BaseParams();
    constructor(
        private readonly dao: BaseDAO<Users>, 
        private readonly mailService: MailService,
        private readonly userDao:UserDao )
    {
        this.userBaseParams.currentRepository = 'Users';
        this.userBaseParams.relations = ['contact']
        this.dao.setBaseParams(this.userBaseParams);
    }

    private sendEmailToUser(user: CreateUserDto, generatedId: string): void{
        const mailToSend: Mail = new Mail();
        mailToSend.mailFrom = 'noreply@arena.com';
        mailToSend.mailTo = user.contact.email;
        mailToSend.mailSubject = 'Welcome to arena',
        mailToSend.mailContext = {
            password: user.password,
            firstName: user.firstName,
            lastName: user.lastName,
            id: generatedId,
            appName: AppConfig.appName
        }
        this.mailService.sendMail(mailToSend);
    }
    
    async addUser(user: CreateUserDto): Promise<GetUserDto> {
        if( !user.contact.email 
            && !user.contact.phoneNumber ){
            throw new HttpException('Please provide an email or a phone number', HttpStatus.BAD_REQUEST);
        }
        const userToCreate: Users = new Users(user);
        const cryptedPass = await bcrypt.hash(user.password,AppConfig.saltRounds);
        const currentUser = await this.userDao.findUserByEmail(
            (user.contact.email)?user.contact.email: user.contact.phoneNumber.toString()
        );
        if(currentUser){
            throw new HttpException("User already exists", HttpStatus.BAD_REQUEST); 
        }
        if(cryptedPass) {
            userToCreate.password = cryptedPass;
            const userId = await bcrypt.hash(Helper.makeId(185),AppConfig.saltRounds)
            userToCreate.id = userId;
            if(user.isArena){
                userToCreate.role = Role.ARENA;
            }
            const createdUser: GetUserDto = new GetUserDto(await this.dao.add(userToCreate));
            if(createdUser){
                /* this.sendEmailToUser(user, userId); */
                return createdUser;
            }
        }
    }

    async updateUser(user: Users): Promise<GetUserDto>{
        return new GetUserDto(await this.dao.update(user, user.pk));
    }

    activateUser(user: Users): void {
        user.isActive = 1;
        this.updateUser(user);
    }

    async validateUser(id: string): Promise<any>{
        const currentUser = await this.dao.findOne({id : id});
        if(!currentUser){
            throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
        }
        currentUser.isActive = 1;
        currentUser.id = null;
        this.updateUser(currentUser);
        return { message: 'Account validated successfuly' }
    }
    
}