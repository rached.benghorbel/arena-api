import { Arena } from "src/arena/entities/arena.entity";
import { BaseEntity } from "src/communs/entities/base-entity";
import { Column, Entity, ManyToOne, OneToOne } from "typeorm";
import { CreateContactDto } from "../dto/contact/create.dto";
import { GetContactDto } from "../dto/contact/get.dto";
import { Users } from "./Users.entity";

@Entity()
export class Contact extends BaseEntity{
    
    @Column({ unique : true, nullable: true })
    phoneNumber: number;
    @Column({unique: true, nullable: true })
    email: string;
    @Column({unique: true, nullable: true})
    fax: string;

    @OneToOne(() => Users, user => user.contact)
    user: Users;

    @ManyToOne(() => Arena, arena => arena.contacts)
    arena: Arena;

    constructor();
    constructor(contact: CreateContactDto | GetContactDto);
    constructor(contact?: CreateContactDto | GetContactDto){
        super();
        this.phoneNumber = (contact) ? contact.phoneNumber: null;
        this.email = (contact) ? contact.email: null;
        this.fax = (contact) ? contact.fax: null;
    }

}