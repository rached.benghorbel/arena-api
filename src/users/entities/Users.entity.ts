import { Entity, Column, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from "src/communs/entities/base-entity";
import { CreateUserDto } from '../dto/users/create.dto';
import { Contact } from './contact.entity';
import { Role } from './role.enum';
import { Reservation } from 'src/arena/entities/reservation.entity';

@Entity()
export class Users extends BaseEntity{

    @Column()
    password: string;
    @Column({ nullable: true })
    firstName: string;
    @Column({ nullable: true })
    lastName: string;
    @Column({default: false, type: "tinyint"})
    isActive: number;
    @Column({ nullable: true })
    id?: string;
    @Column({ default: false })
    isArena: boolean;
    @Column({ default: Role.USER })
    role: Role;
    @OneToOne(() => Contact, contact => contact.user, { cascade: true })
    @JoinColumn()
    contact: Contact;
    @OneToMany( 
        () => Reservation,
        reservation => reservation.reservedBy
    )
    reservations: Array<Reservation>;

    constructor();
    constructor(user: CreateUserDto);
    constructor(user?: CreateUserDto) {
        super();
        this.password = (user) ? user.password: null;
        this.firstName = (user) ? user.firstName: null;
        this.lastName = (user) ? user.lastName: null;
        this.contact = (user) ? new Contact(user.contact): null;
        this.isArena = user?.isArena;
    }

}