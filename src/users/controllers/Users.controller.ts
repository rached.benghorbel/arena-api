import { Controller, Post, Body, Put, Get, Req, Param } from "@nestjs/common";
import { Users } from "../entities/Users.entity";
import { UserService } from "../services/users.service";
import { CreateUserDto } from "../dto/users/create.dto";
import { GetUserDto } from "../dto/users/get.dto";

@Controller('users')
export class UsersController {
    
    constructor(private readonly userService: UserService) {}

    @Post()
    async addUser(@Body() user: CreateUserDto):Promise<GetUserDto>{
        return await this.userService.addUser(user);
    }

    @Put()
    async updateUser(@Body() user: Users): Promise<GetUserDto>{
        return await this.userService.updateUser(user);
    }

    @Get('/activate/:userId')
    async validateUser(@Param('userId') id: string): Promise<any>{
        return this.userService.validateUser(id);
    }

    /* 
    @UseGuards(JwtAuthGuard)
    @Get()
    async listUsers():Promise<Users[]>{
        const usersList: Users[] = await this.userService.findAll();
        return usersList;
    } */
}