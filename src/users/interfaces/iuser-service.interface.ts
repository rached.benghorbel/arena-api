import { Users } from "../entities/Users.entity";
import { CreateUserDto } from "../dto/users/create.dto";
import { GetUserDto } from "../dto/users/get.dto";

export interface IUsersService{
    addUser(user: CreateUserDto): Promise<GetUserDto>;
    updateUser(user: Users): Promise<GetUserDto>;
    validateUser(id: string): Promise<any>;
}