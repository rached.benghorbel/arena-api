import { Type } from "class-transformer";
import { IsNotEmpty, IsOptional, ValidateNested } from "class-validator";
import { CreateContactDto } from "../contact/create.dto";

export class CreateUserDto {
    @IsOptional()
    firstName: string;
    @IsOptional()
    lastName: string;
    @IsNotEmpty()
    password: string;
    @ValidateNested({ each: true })
    @Type(() => CreateContactDto)
    @IsNotEmpty()
    contact: CreateContactDto;
    @IsOptional()
    isArena?: boolean
}