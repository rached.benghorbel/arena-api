import { Type } from "class-transformer";
import { Role } from "src/users/entities/role.enum";
import { Users } from "src/users/entities/Users.entity";
import { GetContactDto } from "../contact/get.dto";

export class GetUserDto {    
    firstName: string;
    lastName: string;
    @Type(() => GetContactDto)
    contact: GetContactDto;
    role: Role;
    hasArena?: boolean;

    constructor();
    constructor(user:Users)
    constructor(user?: Users){
        this.firstName = user?.firstName;
        this.lastName = user?.lastName;
        this.contact = new GetContactDto(user?.contact);
        this.role = user?.role;
    }

}