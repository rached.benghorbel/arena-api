import { IsEmail, IsOptional, IsPhoneNumber } from "class-validator";

export class CreateContactDto{
    @IsOptional()
    @IsEmail()
    email: string;
    @IsOptional() @IsPhoneNumber('TN')
    phoneNumber: number;
    @IsOptional()
    fax?: string;
}