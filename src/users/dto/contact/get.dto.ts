import { Contact } from "src/users/entities/contact.entity";

export class GetContactDto{
    email: string;
    phoneNumber?: number;
    fax?: string;

    constructor();
    constructor(contact: Contact);
    constructor(contact?:Contact){
        this.email = contact?.email;
        this.fax = contact?.fax;
        this.phoneNumber = contact?.phoneNumber;
    }

}