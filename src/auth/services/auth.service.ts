import { IAuthService } from '../interfaces/iauth-service.interface';
import { BaseParams } from 'src/communs/entities/base-params';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { Request } from 'express';
import { GetUserDto } from 'src/users/dto/users/get.dto';
import { BaseDAO } from 'src/communs/dao/base.dao';
import { Users } from 'src/users/entities/Users.entity';
import { UserDao } from 'src/users/dao/user.dao';
import { OtpService } from 'src/communs/services/otp/otp.service';
import { UserService } from 'src/users/services/users.service';
import { MailService } from 'src/communs/services/mail/mail.service';
import { Mail } from 'src/communs/entities/mail-entity';
import { AppConfig } from 'src/app.config';
import { Helper } from 'src/communs/helpers/helper';

@Injectable()
export class AuthService implements IAuthService {
  authParams: BaseParams = new BaseParams();

  constructor(
    private readonly jwtService: JwtService,
    private readonly dao: BaseDAO<Users>,
    private readonly userDao: UserDao,
    private readonly otpService: OtpService,
    private readonly userService: UserService,
    private readonly mailService: MailService
  ) {
    this.authParams.currentRepository = 'Users';
    this.dao.setBaseParams(this.authParams);
  }

  async authenticate(
    login: string,
    password: string,
    otp?: string,
  ): Promise<GetUserDto> {
    const authenticatedUser: Users = await this.userDao.findUserByEmail(login);

    if (!authenticatedUser) {
      throw new HttpException(
        'email or phone number not found',
        HttpStatus.UNAUTHORIZED,
      );
    }

    await this.verifyPassword(password, authenticatedUser.password);

    if (!authenticatedUser.isActive) {
      if (!this.checkOtp(authenticatedUser, otp) && otp) {
        throw new HttpException(
          'Wrong given otp',
          HttpStatus.PRECONDITION_FAILED,
        );
      } else if (!otp) {
        this.sendOtpVerification(authenticatedUser);
        throw new HttpException(
          'Enter the password sent',
          HttpStatus.PRECONDITION_FAILED,
        );
      }
    }

    return authenticatedUser;
  }

  async generateAccessToken(user: Users): Promise<string> {
    const payload: JwtPayload = { username: user.contact.email, sub: user.pk };
    return this.jwtService.sign(payload);
  }

  async checkUser(login: string, req: Request): Promise<Users> {
    try {
      const authenticatedUser: Users = await this.userDao.findUserByEmail(
        login,
      );
      if (authenticatedUser) {
        Helper.currentUser = authenticatedUser;
        return authenticatedUser;
      }
    } catch (Exception) {
      throw new HttpException(Exception.message, HttpStatus.UNAUTHORIZED);
    }
  }

  private async verifyPassword(
    plainTextPassword: string,
    hashedPassword: string,
  ) {
    const isPasswordMatching = await bcrypt.compare(
      plainTextPassword,
      hashedPassword,
    );
    if (!isPasswordMatching) {
      throw new HttpException(
        'Wrong credentials provided',
        HttpStatus.UNAUTHORIZED,
      );
    }
  }

  private checkOtp(authenticatedUser: Users, otp: string): boolean {
    if (!this.otpService.checkOtp(otp)) {
      return false;
    }
    this.userService.activateUser(authenticatedUser);
    return true;
  }

  private sendOtpVerification(user: Users): void {
    const otp: string = this.otpService.generateOtp();
    const mailToSend: Mail = {
      mailTo: user.contact.email,
      mailContext: {
        appName: AppConfig.appName,
        firstName: user.firstName,
        lastName: user.lastName,
        otp: otp,
      },
      mailFrom: 'reserve arena',
      mailSubject: 'Confirmation code',
      mailTemplate: 'otp',
    };
    this.mailService.sendMail(mailToSend);
  }
}
