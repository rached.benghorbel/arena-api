import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';
import { GetUserDto } from 'src/users/dto/users/get.dto';
 
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
      super(
        { 
          usernameField: 'email',
          passReqToCallback: true
        }
      )
    }
  async validate(req:Request, username: string, password: string): Promise<GetUserDto> {
    return this.authService.authenticate(username, password, req.body['otp']);
  }
}