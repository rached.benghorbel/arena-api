import { Request } from "express";
import { GetUserDto } from "src/users/dto/users/get.dto";
import { Users } from "src/users/entities/Users.entity";

export interface IAuthService{
    authenticate(login: string, password: string, otp?: string): Promise<GetUserDto>;
    generateAccessToken(user:Users):Promise<any>;
    checkUser(login: string, req: Request): Promise<Users>;
}