import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AppConfig } from 'src/app.config';
import { AuthController } from './controllers/auth.controller';
import { BaseDAO } from 'src/communs/dao/base.dao';
import { AuthService } from './services/auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './services/local.strategy';
import { JwtStrategy } from './services/jwt.strategy';
import { UserDao } from 'src/users/dao/user.dao';
import { UserService } from 'src/users/services/users.service';
import { MailService } from 'src/communs/services/mail/mail.service';
import { OtpService } from 'src/communs/services/otp/otp.service';
import { ArenaService } from 'src/arena/services/arena/arena.service';

@Module({
    imports: [
        PassportModule.register({      
            defaultStrategy: 'jwt',      
            property: 'user',      
            session: false,    
        }),
        JwtModule.register({
            secret: AppConfig.jwtSecret,
            signOptions: {
              expiresIn: AppConfig.jwtLifeTime,
            },
        }),
    ],
    controllers: [ AuthController ],
    providers:[
        BaseDAO,
        AuthService,
        LocalStrategy,
        JwtStrategy,
        UserDao,
        UserService,
        MailService,
        OtpService,
        ArenaService
    ]
})
export class AuthModule {}
