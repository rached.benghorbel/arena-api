import {
  Controller,
  Post,
  UseGuards,
  HttpCode,
  Request,
  Res,
} from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { LocalAuthenticationGuard } from '../services/localAuthentication.guard';
import { Response } from 'express';
import { GetUserDto } from '../../users/dto/users/get.dto';
import { Users } from 'src/users/entities/Users.entity';
import { ArenaService } from 'src/arena/services/arena/arena.service';
import { ConvertUserMapper } from 'src/users/mappers/convert-user.mapper';
import { Role } from 'src/users/entities/role.enum';

@Controller('authentication')
export class AuthController {
  constructor(
    private readonly authService: AuthService, 
    private readonly arenaService: ArenaService
  ) {}

  @HttpCode(200)
  @UseGuards(LocalAuthenticationGuard)
  @Post()
  async logIn(@Request() request, @Res() response: Response) {
    const user: Users = request.user as Users;
    const userToGet: GetUserDto = ConvertUserMapper.convertUserToGetUserDto(user);
    if(user.role == Role.ARENA) {
      userToGet.hasArena = (await this.arenaService.findArenaByUser(user.pk) != null);
    }
    const jwtToken = await this.authService.generateAccessToken(user);
    response.setHeader('token', jwtToken);
    return response.send(userToGet);
  }
}
