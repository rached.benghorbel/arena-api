import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Mail } from 'src/communs/entities/mail-entity';

@Injectable()
export class MailService {
    constructor(private readonly mailerService: MailerService){ }

    public sendMail(mailToSend: Mail): void{
        this.mailerService.sendMail({
            to: mailToSend.mailTo,
            from: mailToSend.mailFrom,
            subject: mailToSend.mailSubject,
            template: mailToSend.mailTemplate,
            context: mailToSend.mailContext
        })
        .then( (res) => console.log(res) )
        .catch( (err) => console.log(err) );
    }
}
