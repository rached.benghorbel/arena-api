import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { totp } from 'otplib';
import { AppConfig } from 'src/app.config';
import { IOtpService } from './otp.service.interface';

@Injectable()
export class OtpService implements IOtpService {
    
    constructor(private readonly configService: ConfigService){}

    generateOtp(): string {
        totp.options = {
            digits: AppConfig.otpDigits /* this.configService.get<number>('OTP_DIGITS') as number */,
            epoch: Date.now(),
            step: AppConfig.otpStep /* this.configService.get<number>('OTP_LIFETIME') */,
            window: 0,
        }
        return totp.generate(this.configService.get('OTP_SECRET'));
    }
    
    checkOtp(otp: string): boolean {
        return totp.check(otp, this.configService.get('OTP_SECRET'));
    }
}
