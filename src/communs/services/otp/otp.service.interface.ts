export interface IOtpService{
    generateOtp(): string;
    checkOtp(otp: string): boolean;
}