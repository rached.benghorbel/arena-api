import { Users } from "src/users/entities/Users.entity";

export class Helper{

    public static currentUser: Users;

    public static makeId(length: number): string {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    public static convertBlobToBase64(content): string {
        return Buffer.from( content, 'binary').toString('base64');
    }

    public static convertBase64ToBlob(content) {
        return Buffer.from(content,"base64");
    } 

}