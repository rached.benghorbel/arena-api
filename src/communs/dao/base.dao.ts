/***
 * @Author Rached Ben Ghorbel
 * @email benghorbelrached@gmail.com
 * 26-10-2020
 ***/

import { IBaseDAO } from "../interfaces/ibase-service.interface";
import { getRepository, Not } from "typeorm";
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { BaseParams } from "../entities/base-params";
import { BaseEntity } from "../entities/base-entity";
import { EntityStatus } from "../entities/entity-status.enum";

@Injectable()
export class BaseDAO<T extends BaseEntity> implements IBaseDAO<T>{
    repository;
    
    private baseParams: BaseParams;

    public setBaseParams(receivedPramas: BaseParams): void{
        this.baseParams = receivedPramas;
    } 

    async add(entity: T): Promise<T> {
        try{
            this.repository = getRepository(this.baseParams.currentRepository);
            const newEntity = <T>(this.repository.create(entity));
            await this.repository.save(newEntity);
            return newEntity;
        }catch(exception){
            throw new HttpException(exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async update(entity: T, id: string): Promise<T> {
        try{
            this.repository = getRepository(this.baseParams.currentRepository);
            const updatedEntity = <T>(entity);
            await this.repository.save(updatedEntity);
            return this.repository.findOne(id);
        }catch(exception){
            throw new HttpException(exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    delete(id: string): Promise<T> {
        throw new Error("Method not implemented.");
    }
    

    async findOne(params: any, includeRelations = false): Promise<T> {
        try{
            this.repository = getRepository(this.baseParams.currentRepository);
            if(includeRelations)    
                return await this.repository.findOne(
                { 
                    where: params,
                    andWhere: { entityStatus: Not(EntityStatus.DELETED) } ,
                    relations:this.baseParams.relations 
                });
            return await this.repository.findOne({ where: params });
        }catch(exception){
            throw new HttpException(exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }   
    }
    
    async findByParams(params: any, includeRelations = false): Promise<T[]> {
        try {
            this.repository = getRepository<T>(this.baseParams.currentRepository);
            if(includeRelations)    
                return await this.repository.find(
                { 
                    where: params,
                    andWhere: { entityStatus: Not(EntityStatus.DELETED) } ,
                    relations:this.baseParams.relations 
                });
            return await this.repository.find({ 
                where: params,
                andWhere: { entityStatus: Not(EntityStatus.DELETED) }
            });

        } catch(exception){
            throw new HttpException(exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async findAll(includeRelations = false): Promise<T[]> {
        try{
            this.repository = getRepository(this.baseParams.currentRepository);
            
            if(includeRelations)
                return await this.repository.find(
                    { 
                        relations: this.baseParams.relations,
                        where: { entityStatus: Not(EntityStatus.DELETED) } 
                    }
                );

            return await this.repository.find();
        }catch(exception){
            throw new HttpException(exception.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}