export class Mail{
    mailTo: string;
    mailFrom: string;
    mailSubject: string;
    mailTemplate = 'index';
    mailContext: any;
}
