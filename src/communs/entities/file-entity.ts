import { BeforeInsert, BeforeUpdate, Column } from "typeorm";
import { BaseEntity } from "./base-entity";

export class FileEntity extends BaseEntity{
    @Column()
    public name?: string;
    @Column({nullable:true})
    public size?: number;
    @Column({nullable:true})
    public extension?: string;
    @Column({nullable:true})
    public path?: string;
    @Column({type: 'longtext'})
    public content?;
}