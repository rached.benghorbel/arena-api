export enum EntityStatus{
    CREATED = "CREATED",
    UPDATED = "UPDATED",
    DELETED = "DELETED"
}