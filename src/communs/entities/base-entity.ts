import { IBaseEntity } from "../interfaces/ibase-entity.interface";
import { EntityStatus } from "./entity-status.enum";
import { PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate } from "typeorm";
import { Helper } from "../helpers/helper";

export class BaseEntity implements IBaseEntity{
    @PrimaryGeneratedColumn("uuid")
    pk: string;
    @Column()
    @Column({nullable: true})
    created: Date;
    @Column({nullable: true})
    updated?: Date;
    @Column({nullable: true})
    deleted?: Date;
    @Column({nullable: true})
    createdU: string;
    @Column({nullable: true})
    updatedU?: string;
    @Column({nullable: true})
    deletedU?: string;
    @Column({nullable: true})
    entityStatus: EntityStatus;

    @BeforeInsert()
    saveIdentifiers(): void{
        this.created = new Date();
        this.entityStatus = EntityStatus.CREATED;
        this.createdU = Helper.currentUser?.pk;
    }

    @BeforeUpdate()
    updateIdentifiers(): void{
        this.updated = new Date();
        this.entityStatus = EntityStatus.UPDATED;
        this.updatedU = Helper.currentUser?.pk;
    }
}