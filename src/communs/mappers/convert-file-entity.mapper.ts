import { FileEntityDto } from "../dto/file-entity.dto";
import { FileEntity } from "../entities/file-entity";

export abstract class ConvertFileEntityMapper {
    
    public static toDto(file: FileEntity): FileEntityDto {
        const fileEntityDto: FileEntityDto = new FileEntityDto();
        fileEntityDto.pk = file.pk;
        fileEntityDto.name = file.name;
        fileEntityDto.extension = file.extension;
        fileEntityDto.path = file.path;
        fileEntityDto.size = file.size;
        fileEntityDto.content = file.content;
        return fileEntityDto;
    }

    public static toDtoList(files?: Array<FileEntity>) : Array<FileEntityDto> {
        const filesEntityDto: Array<FileEntityDto> = [];
        files?.forEach(file => {
            filesEntityDto.push(this.toDto(file));
        });
        return filesEntityDto;
    } 

} 