export const AppConfig = {
    globalPath: 'arena/api/services',
    portToListen: process.env.PORT || 3000,
    saltRounds: 10,
    jwtSecret: 'p1ou_q69H_I7cYVqd8FecsRxLMC78avXrHVHERfU5VQ',
    jwtLifeTime:'1d',
    appName: 'arena',
    otpDigits: 6,
    otpStep:300
}