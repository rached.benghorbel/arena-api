import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppConfig } from './app.config';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(AppConfig.globalPath);
  app.enableCors({
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
    exposedHeaders: 'token',
    credentials: true,
  });
  app.useGlobalPipes(
      /**
       * Reference: https://docs.nestjs.com/techniques/validation#auto-validation
       */
      new ValidationPipe({
        // Make sure that there's no unexpected data
        whitelist: true,
        forbidNonWhitelisted: true,
        forbidUnknownValues: true,

        /**
         * Detailed error messages since this is 4xx
         */
        disableErrorMessages: false,
        
        validationError: {
          /**
           * WARNING: Avoid exposing the values in the error output (could leak sensitive information)
           */
          value: false,
        },

        /**
         * Transform the JSON into a class instance when possible.
         * Depends on the type of the data on the controllers
         */
        transform: true,
      }),
  );
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
  await app.listen(AppConfig.portToListen);
}
bootstrap();
